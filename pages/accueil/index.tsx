import React, { useRef, useState, useLayoutEffect, useEffect } from "react"
import { NextPage } from 'next'
import { Container, Card, Row, Col } from 'react-bootstrap'
import { Footer, Head, MainHeader, Menu, Section, CounterUpAnimation } from '../../components'
import style from './Style.module.css'
import { ButtonStyle, CardStyle } from '../../styles'
import Slider from 'react-slick'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight, faClock, faLocationDot } from "@fortawesome/free-solid-svg-icons"
import Image from "next/image"
import Link from "next/link"
import { getHref, getUrl, imageLoader, getApiUrl, truncateText, getApiImageUrl } from "../../utils"
import { leaderProps, postProps } from "../../props"
import axios from "axios"

const Home: NextPage = () => {
    const leadersSliderSettings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]

    }

    const partnersSliderSettings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]


    }

    // const [leaders, setLeaders] = useState([]);

    // useEffect(() => {
    //     axios.get(`/api/getleaders`)
    //     .then((res:any) => {
    //         setLeaders(res.data);
    //     })

    // })

    const leadersCards = [
        {
            id: 0,
            image: getUrl('/board/1.png'),
            name: 'Alex Sea',
            poste: 'co-Founder and director, FINOV AFRICA, Côte d’Ivoire'
        },
        {
            id: 1,
            image: getUrl('/board/KATHARINA DAKLA.jpg'),
            name: 'KATHARINA DAKLA',
            poste: 'Fondatrice et PDG de StellarOne - Strategy and Investment advisory in Tech'
        },

        {
            id: 2,
            image: getUrl('/board/Yves komaclo.jpg'),
            name: 'Yves komaclo',
            poste: "Directeur d'Investissement Afrique de l'Ouest (Investment Manager), Oikocredit"
        },

        {
            id: 3,
            image: getUrl('/board/Roseline Emma Rasolovoahangy.jpg'),
            name: 'Roseline Emma Rasolovoahangy',
            poste: 'President petromad Inc'
        },

        {
            id: 4,
            image: getUrl('/board/Bruner Nozière.jpg'),
            name: 'Bruner Nozière',
            poste: 'Co-Founder Netdollar'
        },

        {
            id: 5,
            image: getUrl('/board/Matteo Rizzi.jpg'),
            name: 'Matteo Rizzi',
            poste: 'Venture Partner at Bamboo Capital Partners | Co-Founder at FTSGroup.eu | Published Author - "Fintech Revolution" (2016) and "Talents & Rebels" (2019)'
        },

        {
            id: 6,
            image: getUrl('/board/MARIEME NDIAYE.jpg'),
            name: 'MARIEME NDIAYE',
            poste: 'Senior expert payment Fintech'
        },

        {
            id: 7,
            image: getUrl('/board/Andrew Davis.jpg'),
            name: 'Andrew Davis',
            poste: 'The Fintech Institute Executive Director '
        },

        {
            id: 8,
            image: getUrl('/board/Michael salmony.jpg'),
            name: 'Michael salmony',
            poste: ' equensWorldline SE Germany, Executive Adviser '
        },

        {
            id: 9,
            image: getUrl('/board/Estelle brack.jpg'),
            name: 'Estelle brack',
            poste: 'Founder and CEO of KiraliT '
        },
        {
            id: 10,
            image: getUrl('/board/hafou touré.jpg'),
            name: 'hafou touré',
            poste: 'Founder HTS Partners  '
        },

        {
            id: 11,
            image: getUrl('/board/Yassine Regragui.jpg'),
            name: 'Yassine Regragui',
            poste: 'Fintech & China Expert  ex-Deloitte, Alipay, Alibaba '
        },
        {
            id: 12,
            image: getUrl('/board/Josué Toho.jpg'),
            name: 'Josué Toho',
            poste: 'Inclusive Finance & Fintech Specialist'
        },

        {
            id: 13,
            image: getUrl('/board/Babacar Diallo.jpg'),
            name: 'Babacar Diallo',
            poste: 'Consultant Inclusion Specialist Financial, West Africa region'
        },
        {
            id: 14,
            image: getUrl('/board/Jaures Tcheou.jpg'),
            name: 'Jaures Tcheou',
            poste: 'CEO Capitaux Reunis et AfreeMonex'
        },

        {
            id: 15,
            image: getUrl('/board/Abdelkader yousfi.jpg'),
            name: 'Abdelkader yousfi',
            poste: 'Founder and CEO of sprinthub '
        },
        {
            id: 16,
            image: getUrl('/board/DRAMANE MEITE.jpg'),
            name: 'DRAMANE MEITE',
            poste: "Professionnel de l'investissement et des services financiers"
        },

        {
            id: 17,
            image: getUrl('/board/Sory TOURE.jpg'),
            name: 'Sory TOURE',
            poste: 'CEO DEXTERITY AFRICA'
        },
        {
            id: 18,
            image: getUrl('/board/Devinder Singh.jpg'),
            name: 'Devinder Singh',
            poste: 'ERICSSON, Head of Mobile Money CU MTN West'
        },
        {
            id: 19,
            image: getUrl('/board/Sidi-Mohamed Dhaker.jpg'),
            name: 'Sidi-Mohamed Dhaker',
            poste: 'Conseiller du Gouverneur Banque Centrale de Mauritanie'
        },

        {
            id: 20,
            image: getUrl('/board/Karine Mazand-Mboumba Tchitoula.jpg'),
            name: 'Karine Mazand-Mboumba Tchitoula',
            poste: 'MMT Avocats Avocat-Legal designer'
        },
    ]

    const partnersCards = [
        {
            id: 1,
            image: getUrl('/partners/1.jpeg')
        },

        {
            id: 2,
            image: getUrl('/partners/2.jpeg')
        },

        {
            id: 3,
            image: getUrl('/partners/3.jpeg')
        },

        {
            id: 4,
            image: getUrl('/partners/4.jpeg')
        },

        {
            id: 5,
            image: getUrl('/partners/5.jpeg')
        },

        {
            id: 6,
            image: getUrl('/partners/6.jpeg')
        },

        {
            id: 7,
            image: getUrl('/partners/7.jpeg')
        },

        {
            id: 8,
            image: getUrl('/partners/8.jpeg')
        },

        {
            id: 9,
            image: getUrl('/partners/9.jpeg')
        },

        {
            id: 10,
            image: getUrl('/partners/10.jpeg')
        },

        {
            id: 11,
            image: getUrl('/partners/11.jpeg')
        },

        {
            id: 12,
            image: getUrl('/partners/12.jpeg')
        },
        {
            id: 13,
            image: getUrl('/partners/13.jpeg')
        },
        {
            id: 14,
            image: getUrl('/partners/14.jpeg')
        },
        {
            id: 15,
            image: getUrl('/partners/15.jpeg')
        },
        {
            id: 16,
            image: getUrl('/partners/16.jpeg')
        },
        {
            id: 17,
            image: getUrl('/partners/17.jpeg')
        },
        // {
        //     id: 18,
        //     image: getUrl('/partners/18.jpeg')
        // },
        {
            id: 19,
            image: getUrl('/partners/19.jpeg')
        },


    ]

    const statistics = [
        {
            id: 1,
            title: 'Fintechs créées',
            value: 15
        },

        {
            id: 2,
            title: 'Start-ups formées',
            value: 200
        },

        {
            id: 3,
            title: 'Pays visités',
            value: 20
        },

        {
            id: 4,
            title: 'Membres',
            value: 5000
        }
    ]

    //https://dev.to/chriseickemeyergh/building-custom-scroll-animations-using-react-hooks-4h6f
    const counterRef = useRef<null | HTMLDivElement>(null)
    const [counter, displayCounter] = useState(false)

    useLayoutEffect(() => {
        const topPos = (el: HTMLDivElement | null) => el?.getBoundingClientRect().top
        const counterPos = topPos(counterRef.current) || 0

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;

            if (counterPos < scrollPos) {
                displayCounter(true)
            }
        }

        window.addEventListener("scroll", onScroll);
        return () => window.removeEventListener("scroll", onScroll)
    }, [counter, displayCounter])

    const [leaders, setLeaders] = useState<leaderProps[]>([])

    // useEffect(() => {
    //     axios.get(`/api/getleaders`)
    //     .then((res:any) => {
    //         setLeaders(res.data);
    //         console.log(res.data);

    //     })
    // }, [])

    useEffect(() => {
        fetch(getApiUrl('/api/getleaders'))
            .then(res => res.json())
            .then(data => setLeaders(data))
    }, [])

    const [posts, setPosts] = useState<postProps[]>([])

    useEffect(() => {
        fetch(getApiUrl('/api/headlines'))
            .then(res => res.json())
            .then(data => setPosts(data))
    }, [setPosts])

    return (
        <>
            <Head title='Accueil | AFRICA FINTECH FORUM' />

            <Menu />

            <MainHeader />

            <main>
                <Section>
                    <Container>
                        <h1 className="sectionTitle">A la une <hr /></h1>

                        <Row xs={1} md={3} className="g-5 d-flex justify-content-center align-items-center">
                            {posts.map(post => {
                                if (!post.eventype) {
                                    return (
                                        <Col key={post.id}>
                                            <Card className={CardStyle.main}>
                                                <Card.Img variant="top" src={getApiImageUrl(`/news/${post.image}`)} height={250} />

                                                <Card.Body className="mb-3">
                                                    <Card.Text>
                                                        <small>
                                                            {new Date(post.created_at).toLocaleDateString('fr-FR', { year: 'numeric', day: 'numeric', month: 'short' })} / {post.newstype.name}
                                                        </small>
                                                    </Card.Text>

                                                    <Card.Title className="fw-bold text-uppercase">
                                                        <small>{post.title}</small>
                                                    </Card.Title>

                                                    <Card.Text>
                                                        <small>{truncateText(post.content, 150)}</small>
                                                    </Card.Text>
                                                </Card.Body>

                                                {/* <Link href={getHref(`article/${post.id}`)}> */}
                                                <Link href={getHref(`article/${post.slug}`)}>
                                                    <a className="stretched-link"></a>
                                                </Link>
                                            </Card>
                                        </Col>
                                    )
                                }

                                return (
                                    <Col key={post.id}>
                                        <Card className={CardStyle.main}>
                                            <Card.Img variant="top" src={getApiImageUrl(`/event/${post.image}`)} height={250} />

                                            <Card.Body className="mb-3">
                                                <Card.Text>
                                                    <small>{new Date(post.date).toLocaleDateString('fr-FR', { year: 'numeric', day: 'numeric', month: 'short' })} / {post.eventype.name}</small>
                                                </Card.Text>

                                                <Card.Title className="fw-bold text-uppercase">
                                                    <small>{post.title}</small>
                                                </Card.Title>

                                                <Card.Text>
                                                    <small>
                                                        <div className="d-flex align-items-center">
                                                            <FontAwesomeIcon icon={faLocationDot} />
                                                            <span className="ms-2">{post.location}</span>
                                                        </div>

                                                        <div className="d-flex align-items-center">
                                                            <FontAwesomeIcon icon={faClock} />
                                                            <span className="ms-2">{post.time} UTC</span>
                                                        </div>
                                                    </small>
                                                </Card.Text>
                                            </Card.Body>

                                            <Link href={getHref(`evennement/${post.slug}`)}>
                                                <a className="stretched-link"></a>
                                            </Link>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>

                        <div className="my-5 d-flex justify-content-center">
                            <Link href={getHref("actualite")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">En découvrir plus</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section>

                <Section>
                    {/* <div className={style.block}> */}
                    <div className={style.block}>
                        <div className="mx-auto px-5 py-4" ref={counterRef}>
                        {/* <div className="mx-auto px-5 py-4" ref={counterRef}> */}
                            <h4 className="sectionTitle text-white mb-5">Africa Fintech Forum c&apos;est <hr /></h4>

                            <Row>
                                {counter && statistics.map((statistic, index) => {
                                    return (
                                        <Col key={statistic.id} className="d-flex align-items-center">
                                            <Card className={"border-start-0 border-top-0 border-bottom-0 rounded-0 w-100 " + (index == 3 ? "border-end-0" : "")} style={{ background: 'none', color: 'white', borderColor: 'white' }}>
                                                <Card.Body className="text-center">
                                                    <Card.Title className="fs-1 fw-bold">
                                                        + <CounterUpAnimation countTo={statistic.value}></CounterUpAnimation>
                                                    </Card.Title>
                                                    <Card.Text>{statistic.title}</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    )
                                })}
                            </Row>
                        </div>

                        <div style={{ top: '14em' }} className="">
                            <Container  className={style.back} >
                                <h4 className="sectionTitle text-white my-5">Nos pôles d&apos;activités <hr /></h4>

                                <Row className="text-white">
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFF.png')} layout="fixed" width={150} height={150} alt="AFRICA FINTECH FORUM" loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICA FINTECH FORUM</Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Forum annuel rassemblant les fintech
                                                        acteurs de l&apos;industrie de tous
                                                        continents.</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFF tour.png')} layout="fixed" width={150} height={150} alt="AFRICA FINTECH TOUR " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICA FINTECH TOUR </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Parcours panafricain de promotion de la fintech et d’échange avec les acteurs des différents pays</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/africa fintech forum.png')} layout="fixed" width={150} height={150} alt="THE AFRICA FINTECH FORUM " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">THE AFRICA FINTECH FORUM </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Événement  annuel rassemblant les professionnels de la fintech sur 2 jours</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>

                                </Row>
                                <Row className="text-white">
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/Open Africa.png')} layout="fixed" width={150} height={150} alt="OPEN BANKING AFRICA " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">OPEN BANKING AFRICA </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Organisation à but non lucratif de promotion de l’open banking et de développement de standard </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/cartographie.png')} layout="fixed" width={150} height={150} alt="CARTOGRAPHIE DES ECOSYSTEMES FINTECH  " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">CARTOGRAPHIE DES ECOSYSTEMES FINTECH  </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Etude couvrant 24 pays d’Afrique francophone conduite avec Deloitte et MSC</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/forex.png')} layout="fixed" width={150} height={150} alt="FOREX CLUB UEMOA et FOREX CLUB CEMAC  " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">FOREX CLUB UEMOA et FOREX CLUB CEMAC  </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Réseau associatif des professionnels du change</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>

                                </Row>
                                <Row className="text-white">
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFRICA ISLAMIC FINTECH HUB.png')} layout="fixed" width={150} height={150} alt="AFRICA ISLAMIC FINTECH HUB " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICA ISLAMIC FINTECH HUB </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Hub digital dédié à la finance islamique </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/club.png')} layout="fixed" width={150} height={150} alt="CLUB INNOVATE AFRICA" loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">CLUB INNOVATE AFRICA</Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Événement de réseautage et de promotion des acteurs de l’innovation en Afrique organisé en partenariat avec INWI </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/FINOV AFRICA.png')} layout="fixed" width={150} height={150} alt="FINOV AFRICA" loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">FINOV AFRICA</Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Entreprise de conseil et d’investissement dédié à la finance</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>

                                </Row>

                                <Row className="text-white">
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/BREAKING BANKS AFRICA.png')} layout="fixed" width={150} height={150} alt="BREAKING BANKS AFRICA " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">BREAKING BANKS AFRICA </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Podcast dédié à l’industrie de la finance digitale </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFRICAM ACADEMY.png')} layout="fixed" width={150} height={150} alt="AFRICAM ACADEMY " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICAM ACADEMY </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Entreprises dédiée au développement de l’industrie capital market </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFRICA CAPITAL MARKET FORUM.png')} layout="fixed" width={150} height={150} alt="AFRICA CAPITAL MARKET FORUM  " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICA CAPITAL MARKET FORUM  </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Forum international sur le capital market</span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>

                                </Row>
                                <Row className="text-white">
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/AFRICAM TOUR.png')} layout="fixed" width={150} height={150} alt="AFRICAM TOUR" loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">AFRICAM TOUR</Card.Title>
                                                <Card.Text className="d-flex flex-column text-center mb-5">
                                                    <span>Compétition zone /pays des acteurs du capital market </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/FINTECH VILLAGE.png')} layout="fixed" width={150} height={150} alt="FINTECH VILLAGE  " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">FINTECH VILLAGE  </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center">
                                                    <span>Hub d’innovation continental dédié à la Fintech  </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                    <div className="col-md-4">
                                        <Card className="border-0" style={{ background: 'none' }}>
                                            <div className="mx-auto">
                                                <Image src={getUrl('/pole-accueil/SMART VILLAGE.png')} layout="fixed" width={150} height={150} alt="	SMART VILLAGE   " loader={imageLoader} />
                                            </div>
                                            <Card.Body className="text-center">
                                                <Card.Title className=" fw-bold text-uppercase">SMART VILLAGE   </Card.Title>
                                                <Card.Text className="d-flex flex-column text-center">
                                                    <span>Projet de développement d’écosystème numérique en zone rurale </span>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>

                                </Row>
                            </Container>
                        </div>
                    </div>
                </Section>

                <div  className="container">
                    <h1 className="sectionTitle">Notre board <hr /></h1>

                    <div  className="container">
                    {/* <div style={{ paddingInline: '20em' }}> */}
                        <Slider {...leadersSliderSettings}>
                            {leadersCards.map(leader => {
                                return (
                                    <div key={leader.id}>
                                        <Card className={style.sliderCard} style={{ marginInline: '2em' }}>
                                            <Card.Img variant="top" src={leader.image}  />
                                            {/* <Card.Img variant="top" src={getApiImageUrl(`/leader/${leader.image}`)} height={300} /> */}
                                            <div className={style.sliderCardDescription}>
                                                <div className="text-center">
                                                    <p className="mb-0 fs-5 fw-bold">{leader.name}</p>
                                                    <p className="fs-6">{leader.poste}</p>
                                                </div>
                                            </div>
                                        </Card>
                                    </div>
                                )
                            })}
                        </Slider>
                    </div>
                </div>

                <Section style={{ marginBottom: '10em', marginTop: '8em' }} className="container">
                    <h1 className="sectionTitle">Nos partenaires <hr /></h1>

                    <div className="container">
                    {/* <div style={{ paddingInline: '20em' }}> */}
                        <Slider {...partnersSliderSettings}>
                            {partnersCards.map(partner => {
                                return (
                                    <div key={partner.id}>
                                        <Card className={style.sliderCard + " px-5 border-0"}>
                                            <Card.Img variant="top" src={partner.image}  />
                                            {/* <img src={partner.image} alt="" height={230} width={250} /> */}
                                        </Card>
                                    </div>
                                )
                            })}
                        </Slider>
                    </div>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Home
