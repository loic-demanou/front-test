import { NextPage } from 'next'
import React, { useState, useEffect } from 'react'
import { Card, Container, Row, Col } from 'react-bootstrap'
import { Footer, Menu, Head, Header, Section } from '../../components'
import style from './Style.module.css'
import { faAngleRight, faUser, faFaceLaughBeam, faMessage, faArrowDown } from "@fortawesome/free-solid-svg-icons"
import { faWordpress, faXbox, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { getHref, getApiImageUrl, getApiUrl, getUrl, imageLoader } from '../../utils'
import Link from 'next/link'
import { ButtonStyle, CardStyle } from '../../styles'
import { postProps } from '../../props'
import Image from 'next/image'

const Missions: NextPage = () => {
    const missionsCard = [
        {
            id: 1,
            image: getUrl('/mission/fintech industry mapping.png') ,
            title: 'Fintech Industry Mapping',
            content: "Projet d'identification des acteurs des écosystèmes Fintech Pays africains francophones ou ayant le français avoir une deuxième langue"
        },

        {
            id: 2,
            image: getUrl('/mission/islamique fintech hub.png'),
            title: 'Africa Islamic Fintech Hub',
            content: "Pôle de développement fintech islamique et pool de collaboration et opportunités pour les acteurs de l'ndustrie fintech en Afrique"
        },

        {
            id: 3,
            image: getUrl('/mission/africa fintech chair.png'),
            title: 'African Fintech Chair',
            content: "Chaire de recherche et de formation regroupant des enseignants-chercheurs africains pour impliquer l'université dans le développement de l'industrie Fintech"
        },

        {
            id: 4,
            image: getUrl('/mission/fintech village.png'),
            title: 'Fintech village',
            content: "Continental Tech Hub dédié aux Fintech, un pool de collaboration, d'incubation et d'accélération de technologies innovantes et inclusives."
        },

        {
            id: 5,
            image: getUrl('/mission/africa fintech day.png'),
            title: 'Africa Fintech Day',
            content: "Journée promotionnelle et pédagogique sur la Fintech pour étudiants d'universités, d'écoles de commerce et d'Afrique population"
        },

        {
            id: 6,
            image: getUrl('/mission/open banking africa.png'),
            title: 'Open Banking Africa',
            content: "Initiative pour accélérer la collaboration entre acteurs de l'industrie Fintech et promouvoir la développement de la finance numérique."
        }
    ]

    const statistiquesCards = [
        {
            id: 1,
            title: 'Pays visités',
            value: 20
        },

        {
            id: 2,
            title: 'Intervenants',
            value: 100
        },

        {
            id: 3,
            title: 'USD',
            value: '5M'
        },

        {
            id: 4,
            title: 'Followers',
            value: 5000
        },

        {
            id: 5,
            title: 'Participants',
            value: 5000
        },

        {
            id: 6,
            title: 'Fintechs',
            value: 15
        },

        {
            id: 7,
            title: 'Startups',
            value: 200
        },

        {
            id: 8,
            title: 'Formations avancées',
            value: 5
        },
    ]
    
    const [page, setPage] = useState(1)
    // const [page, setPage] = useState(1)

    const [missions, setMissions] = useState<{
        data: Array<postProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    // const getMissions = () => {
    //     setPage(page => page + 1)

    //     fetch(getApiUrl(`/api/missions?page=${page}&limit=3`))
    //         .then(res => res.json())
    //         .then(data => setMissions({
    //             ...missions,
    //             data: [...missions.data, ...data.data],
    //             next_page_url: data.next_page_url
    //         }))
    // }

    useEffect(() => {
        fetch(getApiUrl(`/api/missions?limit=3`))
            .then(res => res.json())
            .then(data => setMissions(data))
    }, [setMissions])

    return (
        <>
            <Head title='Missions | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Missions"
            description="Fintech Africa est une initiative visant à soutenir le développement et la croissance du réseau Africa Fintech Network (AFN) et à introduire un écosystème de soutien numérique aux fintechs à travers le continent. La restructuration de l'AFN en une entreprise sociale, avec des activités durables, génératrices de revenus et inclusives par nature, garantira la continuité du réseau au-delà des subventions et renforcera son impact."
            />

            <main>
                <Section>
                    <Container>
                        <h1 className="sectionTitle">Nos missions <hr /></h1>

                        <Row xs={1} md={2} lg={3} className="g-5">
                            {missionsCard.map(mission => {
                                return (
                                    <Col key={mission.id}>
                                        <Card className={CardStyle.transparent + " text-center"}>
                                            <Card.Body>
                                                {/* <FontAwesomeIcon icon={mission.image} size="3x" className="mb-3" style={{ color: 'var(--primary-color)' }} /> */}
                                                
                                                {/* <img src={mission.image} className={CardStyle.image} layout="fixed" width={150} height={150} alt="AFRICA FINTECH FORUM" loader={imageLoader} /> */}
                                              
                                                <Image src={mission.image} className={CardStyle.image} layout="fixed" width={150} height={150} alt="AFRICA FINTECH FORUM" loader={imageLoader} />

                                                <Card.Title style={{ color: 'var(--primary-color)' }}>
                                                    {mission.title}
                                                    
                                                    <hr style={{
                                                        width: '2em',
                                                        height: '5px',
                                                        margin: '0 auto',
                                                        marginBlock: '1em',
                                                        opacity: 1,
                                                    }} />
                                                </Card.Title>
                                                <Card.Text>{mission.content}</Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </Container>
                </Section>

                <Section className={style.statistiques}>
                    <Container>
                        <h1 className="sectionTitle mb-5 text-white">Nos missions réalisées <hr /></h1>

                        <p className="text-center text-white">
                        Depuis 2017, AFF a toujours eu à cœur   de construire pour l’Afrique un écosystème fintech basé sur le pavage et le renforcement du 
                        réseau local qui rassemble tous les acteurs de l’industrie avec ses partenaires de partout dans le monde.
                        </p>

                        <h5 className="sectionTitle my-5 text-white"><hr /></h5>
                        
                    </Container>

                    {/* <Row className="px-5">
                        {statistiquesCards.map((statistique, index) => {
                            return (
                                <Col key={statistique.id} className="d-flex align-items-center">
                                    <Card className={"border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 7 ? "border-end-0" : "")} style={{ background: 'none', color: 'white', borderColor: 'var(--primary-color)' }}>
                                        <Card.Body className="text-center">
                                            <Card.Title className="fs-2 fw-bold ">+ {statistique.value}</Card.Title>
                                            <Card.Text><span className="text-uppercase ">{statistique.title}</span></Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row> */}

                    <Container>
                        <Row xs={1} md={2} lg={3} className="g-5 mt-3 d-flex justify-content-center align-items-center">
                            {missions.data.map(mission => {
                                return (
                                    <Col key={mission.id}>
                                        <Card className={CardStyle.main + " shadow border-start-0 border-top-0 border-top-0"}>
                                            <Card.Img variant="top" src={getApiImageUrl(`/mission/${mission.image}`)} height={250} />
                                            
                                            <Card.Body className="mb-3">
                                                <Card.Title className="fw-bold text-primary text-uppercase">
                                                    <small>{mission.title}</small>
                                                </Card.Title>

                                                <Card.Text>
                                                    <small>{mission.content}</small>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )})
                            }
                        </Row>
                    </Container>
                </Section>

                <Section className={style.block}>
                    <Container className="text-center">
                        <div className={style.ceoSection}>
                            <h2 style={{ color:"#FFFF" }} >Participez aux missions<hr /></h2>
                            <h1 className='text-white'>Pourquoi nous rejoindre ?</h1>

                            <p>
                            Depuis sa creation en 2017 AFF c&apos;est +20 Countries visited to meet ecosystem local players, +100 Speakers from Africa,America,Asia and Europe, USD +5, 000 ,000 of financial flows stimulated by our projects, +5000 Players from fintech community connected to our platforms, +5,000Participants regulators, banks, insurances, fintechs, MNOs, investors, etc., +15 Fintechs creation inspired during Africa Fintech Forum, +200 Startups entrepreneurs, intrapreneurs trained, +5 Advanced training provided to entrepreneurs and intrapreneurs in fintech… Ces initiatives sont soit issues d&apos;Africa Fintech Forum, soit de partenaires avec lesquels nous unissons nos efforts.                            </p>

                            <Link href={getHref("partenaires-aff")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">Participer aux missions</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Missions
