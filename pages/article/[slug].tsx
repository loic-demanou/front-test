import { GetServerSideProps, GetStaticPaths, GetStaticProps, NextPage } from "next"
import Image from "next/image"
import React, { useState, useEffect } from "react"
import { Container, Row, Col, Card } from "react-bootstrap"
import { Footer, Menu, Head, Header, Section } from "../../components"
import style from './Style.module.css'
import Link from "next/link"
import { getHref, imageLoader, getApiUrl, getApiImageUrl, truncateText } from "../../utils"
import { CardStyle } from "../../styles"
import { postProps } from "../../props"

const Post: NextPage<{ post: postProps }> = ({ post }) => {
    const [posts, setPosts] = useState<{
        data: Array<postProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    useEffect(() => {
        fetch(getApiUrl(`/api/news?limit=3`))
            .then(res => res.json())
            .then(data => setPosts(data))
    }, [setPosts])

    return (
        <>
            <Head title={`${post.title} | AFRICA FINTECH FORUM`} />

            <Menu />

            <Header
                title={post.title}
                description={new Date(post.created_at).toLocaleDateString('fr-FR', { year: 'numeric', day: 'numeric', month: 'short' }) + ' / ' + post.newstype.name}
            />

            <main>
                <Section>
                    <Container>
                        <div className={style.block}>
                            <Image src={getApiImageUrl(`/news/${post.image}`)} layout="fixed" loader={imageLoader} width={300} height={300} alt={post.title} />
                            <div className={style.blockText} style={{ textAlign: 'justify' }}>{post.content}</div>
                        </div>
                    </Container>
                </Section>

                {posts.data.length > 1 && <Section>
                    <Container>
                        <h5 className="sectionTitle">Voir plus <hr /> </h5>

                        <Row xs={1} md={2} lg={3} className="g-5 d-flex justify-content-center align-items-center">
                            {posts.data.map(_post => {
                                if (post.id != _post.id) {
                                    return (
                                        <Col key={_post.id}>
                                            <Card className={CardStyle.main}>
                                                <Card.Img variant="top" src={getApiImageUrl(`/news/${_post.image}`)} height={250} />
                                                
                                                <Card.Body className="mb-3">
                                                    <Card.Text>
                                                        <small>{new Date(_post.created_at).toLocaleDateString('fr-FR', {year: 'numeric', day: 'numeric', month: 'short'})}</small>
                                                    </Card.Text>
    
                                                    <Card.Title className="fw-bold text-uppercase">
                                                        <small>{_post.title}</small>
                                                    </Card.Title>
    
                                                    <Card.Text>
                                                        <small>{truncateText(_post.content, 150)}</small>
                                                    </Card.Text>
                                                </Card.Body>
                
                                                <Link href={getHref(`article/${_post.slug}`)}>
                                                    <a className="stretched-link"></a>
                                                </Link>
                                            </Card>
                                        </Col>
                                    )}
                                })
                            }
                        </Row>
                    </Container>
                </Section>}
            </main>

            <Footer />
        </>
    )
}

// export const getStaticPaths: GetStaticPaths = async () => {
//     const res = await fetch(getApiUrl('/api/news'))
//     const posts: Array<postProps> = await res.json()

//     const paths = posts.map(post  => ({
//         params: { slug: post.slug }, 
//     }))
    
//     return { paths, fallback: 'blocking' }
// }

// export const getStaticProps: GetStaticProps = async ({ params }) => {
//     const res = await fetch(getApiUrl(`/api/news/${params?.slug}`))
//     const post = await res.json()

//     return {
//         props: { post }
//     }
// }

export default Post

export const getServerSideProps:GetServerSideProps= async ({params}) => {
    const post = await fetch(getApiUrl(`/api/news/${params?.slug}`)).then(r => r.json())

        return {
            props: {
                post
            }
        }
        
    }
