import { NextPage } from 'next'
import React from 'react'
import { Container, Row, Col, Card, Form, Button, Carousel } from "react-bootstrap"
import { Footer, Head, Header, Menu, Section } from '../../components'
import style from './Style.module.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import Link from "next/link"
import { getHref, getUrl, imageLoader } from "../../utils"
import Image from 'next/image'
import Slider from 'react-slick'
import { ButtonStyle, CardStyle } from '../../styles'

const Partners: NextPage = () => {
    const partnersSliderSettings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
    }

    const partnersPlatCards = [
        {
            id: 1,
            url:"https://www.uniskip.store/",
            image: getUrl('/partners/platinium/1.jpg')
        },
        {
            id: 2,
            url:"https://www.wave.com/fr/",
            image: getUrl('/partners/platinium/2.jpg')
        },
    ]
    const partnersGoldCards = [
        {
            id: 1,
            url:"https://www.dataprotect.fr/fr/",
            image: getUrl('/partners/gold/1.jpg')
        },
        {
            url:"https://www2.deloitte.com/afrique/fr/",
            id: 2,
            image: getUrl('/partners/gold/2.jpg')
        },
        {
            url:"https://www.visa.fr/",
            id: 3,
            image: getUrl('/partners/gold/3.jpg')
        },
        {
            url:"https://www.hps-worldwide.com/",
            id: 4,
            image: getUrl('/partners/gold/4.jpg')
        },
        {
            url:"",
            id: 5,
            image: getUrl('/partners/gold/5.jpg')
        },
        {
            url:"",
            id: 6,
            image: getUrl('/partners/gold/6.jpg')
        },
        {
            url:"",
            id: 7,
            image: getUrl('/partners/gold/7.jpg')
        },
        {
            url:"",
            id: 8,
            image: getUrl('/partners/gold/8.jpg')
        },
    ]
    const partnersBronzeCards = [
        {
            id: 1,
            url:"https://paydunya.com/",
            image: getUrl('/partners/bronze/1.jpg')
        },
        {
            id: 2,
            url:"https://adec-ci.com/",
            image: getUrl('/partners/bronze/2.jpg')
        },
        {
            id: 3,
            url:"https://ezipay.africa/sn/",
            image: getUrl('/partners/bronze/3.jpg')
        },
        {
            id: 4,
            url:"https://www.advanscotedivoire.com/",
            image: getUrl('/partners/bronze/4.jpg')
        },
        {
            id: 5,
            url:"https://www.oikocredit.coop/fr/",
            image: getUrl('/partners/bronze/5.jpg')
        },
        {
            id: 6,
            url:"https://omnisliber.org/",
            image: getUrl('/partners/bronze/6.jpg')
        },
        {
            id: 7,
            url:"http://digital54nd.com/",
            image: getUrl('/partners/bronze/7.jpg')
        },
        {
            id: 8,
            url:"https://boasoncc.com/",
            image: getUrl('/partners/bronze/8.jpg')
        },
        {
            id: 9,
            url:"https://julaya.co/",
            image: getUrl('/partners/bronze/9.jpg')
        },
        {
            id: 10,
            url:"https://www.wizallmoney.com/accueil",
            image: getUrl('/partners/bronze/10.jpg')
        },
        {
            id: 11,
            url:"https://connect.myriadgroup.com/",
            image: getUrl('/partners/bronze/11.jpg')
        },
    ]

    const partnersMediaCards = [
        {
            url:"https://www.medi1tv.com/fr",
            id: 1,
            image: getUrl('/partners/media/1.jpg')
        },
        {
            url:"https://www.canalplusadvertising.com/",
            id: 2,
            image: getUrl('/partners/media/2.jpg')
        },
        {
            url:"https://www.financialafrik.com/",
            id: 3,
            image: getUrl('/partners/media/3.jpg')
        },
        {
            url:"https://epistrophe.ci/",
            id: 4,
            image: getUrl('/partners/media/4.jpg')
        },

    ]
    const partnersTransportCards = [
        {
            id: 1,
            url:"https://www.brusselsairlines.com/",
            image: getUrl('/partners/transport/1.jpg')
        },
        {
            id: 2,
            url:"https://www.turkishairlines.com/",
            image: getUrl('/partners/transport/2.jpg')
        },
        {
            id: 3,
            url:"https://www.uber.com/fr/fr/",
            image: getUrl('/partners/transport/3.jpg')
        },
        {
            id: 4,
            url:"https://www.aircotedivoire.com/",
            image: getUrl('/partners/transport/4.jpg')
        },

    ]

    const partnersCards = [
        {
            id: 1,
            image: getUrl('/partners/1.png')
        },
        {
            id: 1,
            image: getUrl('/partners/1.png')
        },
        {
            id: 1,
            image: getUrl('/partners/1.png')
        },
    ]

    const eventsCards = [
        {
            id: 1,
            title: 'Magadascar Fintech Forum 2022',
            image: getUrl('/a-la-une/2.jpeg'),
            location: 'Madagascar, Université d\'Antananarivo',
            time: '09:30',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
        },

        {
            id: 2,
            title: 'Africa Fintech Forum',
            image: getUrl('/a-la-une/3.jpg'),
            location: 'Webinar',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        },

        {
            id: 3,
            title: 'Smart Conference',
            image: getUrl('/a-la-une/4.jpg'),
            location: 'Ethiopie, Addis-Abeba',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        }
    ]

    const youtubeCards = [
        {
            id: 1,
            title: 'Resumé de la conference de presse AFF',
            link: 'https://www.youtube.com/embed/tCc9gudE7HM'
        },

        {
            id: 2,
            title: 'Abidjan Fintech Tour Keynote Rita OULAI Impact Réel de la Fintech en Côte dIvoire',
            link: 'https://www.youtube.com/embed/3hhcL0FAf-8'
        },

        {
            id: 3,
            title: 'Africa Fintech Forum',
            link: 'https://www.youtube.com/embed/Vw7cFFmpbJc'
        }
    ]

    const adsFirstSlides = [
        {
            id: 1,
            image: getUrl('/partners/first-card/1.jpeg'),
            alt: 'First slide'
        },

        {
            id: 2,
            image: getUrl('/partners/first-card/2.jpeg'),
            alt: 'Second slide'
        },
        {
            id: 3,
            image: getUrl('/partners/first-card/3.jpeg'),
            alt: 'Second slide'
        },
    ]

    const adsSecondSlides = [
        {
            id: 1,
            image: getUrl('/partners/second-card/1.jpeg'),
            alt: 'First slide'
        },

        {
            id: 2,
            image: getUrl('/partners/second-card/2.jpeg'),
            alt: 'Second slide'
        },
        {
            id: 3,
            image: getUrl('/partners/second-card/3.jpeg'),
            alt: 'Second slide'
        },
    ]

    const adsThirtSlides = [
        {
            id: 1,
            image: getUrl('/partners/thirt-card/1.jpeg'),
            alt: 'First slide'
        },
    ]
    // const adsSlides = [
    //     {
    //         id: 1,
    //         image: getUrl('/a-la-une/1.jpg'),
    //         alt: 'First slide'
    //     },

    //     {
    //         id: 2,
    //         image: getUrl('/a-la-une/2.jpeg'),
    //         alt: 'Second slide'
    //     }
    // ]

    return (
        <>
            <Head title='Partenaires AFF | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Partenaires AFF" description="Bienvenue pour imaginer demain ensemble" />

            <main>
                <Section>
                    <Container>
                        <p className="fw-bold text-center">
                            Pendant toute l&apos;année des forums, conférences et ateliers sont organisés d&apos;un peu partout dans le monde. Les décideurs internationaux les plus influents du continent se retrouvent pour prendre part à ces moments uniques. Et si, vous aussi, vous saisissiez cette occasion pour faire émerger votre marque et vos actions auprès de ce public hautement qualifié ?
                        </p>
                    </Container>
                </Section>

                <Section>
                    <Container>
                        <h1 className="sectionTitle">Nos partenaires <hr /></h1>

                        <div className="text-center">
                            <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Platinium</h5>

                            <Row className="mb-5" style={{ paddingInline: '30em' }}>
                                {partnersPlatCards.map((partner, index) => {

                                    return (
                                        <Col key={partner.id} className="px-0">
                                            <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                 <Link href={partner.url}>
                                                    <a target="_blank">
                                                        {/* <Image src={partner.image} height={150} width={250} loader={imageLoader}/> */}
                                                        <Image src={partner.image} width={150} height={150} alt="Logo" loader={imageLoader} />
                                                    </a>
                                                </Link>
                                            </Card>
                                        </Col>
                                        )

                                })}
                            </Row>

                            <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Gold</h5>

                            <Row className="mb-5" style={{ paddingInline: '25em' }}>
                                {partnersGoldCards.map((partner, index) => {
                                    if (index % 2 != 0) {
                                        return (
                                            <Col key={partner.id} className="px-0">
                                                <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                <Link href={partner.url}>
                                                    <a target="_blank">
                                                        {/* <Image src={partner.image} height={150} width={150} loader={imageLoader}/> */}
                                                        <Image src={partner.image} width={150} height={150} alt="Logo" loader={imageLoader} />

                                                    </a>
                                                </Link>
                                                </Card>
                                            </Col>
                                        )
                                    }
                                })}
                            </Row>

                            <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Bronze</h5>

                            <div className="mb-5" style={{ paddingInline: '20em' }}>
                                <Slider {...partnersSliderSettings}>
                                    {partnersBronzeCards.map((partner, index) => {
                                        return (
                                            <div key={partner.id}>
                                                <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                <Link href={partner.url}>
                                                    <a target="_blank">
                                                        {/* <Card.Img variant="top" src={partner.image} height={100} /> */}
                                                        <Image src={partner.image} width={150} height={150} alt="Logo" loader={imageLoader} />
                                                    </a>
                                                </Link>
                                                </Card>
                                            </div>
                                        )
                                    })}
                                </Slider>
                            </div>

                            {/* <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Partenaires institutionnels</h5>

                            <Row className="mb-5" style={{ paddingInline: '25em' }}>
                                {partnersCards.map((partner, index) => {
                                    if (index % 2 == 0) {
                                        return (
                                            <Col key={partner.id} className="px-0">
                                                <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                    <Card.Img variant="top" src={partner.image} height={100} />
                                                </Card>
                                            </Col>
                                        )
                                    }
                                })}
                            </Row> */}

                            <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Partenaires média</h5>

                            <Row className="mb-5" style={{ paddingInline: '30em' }}>
                                {partnersMediaCards.map((partner, index) => {
                                    if (index % 2 != 0) {
                                        return (
                                            <Col key={partner.id} className="px-0">
                                                <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                <Link href={partner.url}>
                                                    <a target="_blank">
                                                        {/* <Card.Img variant="top" src={partner.image} height={100} /> */}
                                                        <Image src={partner.image} width={150} height={150} alt="Logo" loader={imageLoader} />
                                                    </a>
                                                </Link>
                                                </Card>
                                            </Col>
                                        )
                                    }
                                })}
                            </Row>

                            <h5 className="mb-4 fs-5 opacity-50 fw-bold text-uppercase">Partenaires transport</h5>

                            <div className="mb-5" style={{ paddingInline: '25em' }}>
                                <Slider {...partnersSliderSettings}>
                                    {partnersTransportCards.map((partner, index) => {
                                        return (
                                            <div key={partner.id}>
                                                <Card className={"px-3 border-start-0 border-top-0 border-bottom-0 rounded-0 " + (index == 5 ? "border-end-0" : "")} style={{ background: 'none' }}>
                                                <Link href={partner.url}>
                                                    <a target="_blank">
                                                        {/* <Card.Img variant="top" src={partner.image} height={100} /> */}
                                                        <Image src={partner.image} width={150} height={150} alt="Logo" loader={imageLoader} />
                                                    </a>
                                                </Link>
                                                </Card>
                                            </div>
                                        )
                                    })}
                                </Slider>
                            </div>
                        </div>
                    </Container>
                </Section>

                <Section>
                    <Container style={{ paddingInline: '5em' }}>
                        <p className="fw-bold text-center mb-5">
                            Nous ne cessons d&apos;agrandir notre réseau pour toucher plus de secteurs.

                            <hr style={{
                                width: '4em',
                                height: '5px',
                                margin: '0 auto',
                                marginTop: '.8em',
                                opacity: 1,
                            }} />
                        </p>

                        <Row xs={1} className="g-5">
                            <Col >
                                <div className="d-flex align-items-center">
                                    <Carousel className="projectCarousel w-100" interval={3000}>
                                        {adsFirstSlides.map(ads => {
                                            return (
                                                <Carousel.Item key={ads.id}>
                                                    <div className="rounded-0" style={{ height: '350px' }}>
                                                        <Image src={ads.image} alt={ads.alt} layout="fill" loader={imageLoader} />
                                                    </div>
                                                </Carousel.Item>
                                            )
                                        })}
                                    </Carousel>

                                    <Card className={CardStyle.horizontal + " shadow w-100"} style={{ height: '350px' }}>
                                        <Card.Body className="p-4">
                                            <div>
                                                <Card.Title className="fw-bold text-uppercase text-primary">
                                                    Mettez en avant votre entreprise lors du plus grand rassemblement d&apos;affaires d&apos;Afrique
                                                </Card.Title>

                                                <Card.Text style={{ textAlign: 'justify' }} className="mt-3">
                                                    Vous souhaitez cultiver vos relations d&apos;affaires, trouver des partenaires financiers, partager votre expertise ou pénétrer de nouveaux marchés ? Les Forums annuel de AFRICA FINTECH FORUM vous offre une visibilité multiple:

                                                    <ul>
                                                        <li>Un stand dédié dans la zone d&apos;exposition du Forum</li>
                                                        <li>Votre logo sur tous les outils de communication de l&apos;événement (site web, newsletters d&apos;information, publicités...)</li>
                                                        <li>Une page de publicité dans le catalogue officiel du Forum</li>
                                                    </ul>
                                                </Card.Text>
                                            </div>
                                        </Card.Body>
                                    </Card>
                                </div>
                            </Col>

                            <Col>
                                <div className="d-flex align-items-center">
                                    <Card className={CardStyle.horizontal + " shadow w-100"} style={{ height: '350px' }}>
                                        <Card.Body className="p-4">
                                            <div>
                                                <Card.Title className="fw-bold text-uppercase text-primary">
                                                    Rencontrez les CEOs, les investisseurs et les médias les plus influents d&apos;Afrique
                                                </Card.Title>

                                                <Card.Text style={{ textAlign: 'justify' }} className="mt-5">
                                                    Vous désirez entrer en contact avec certains décideurs  Nos équipes vous aident à  participer aux sessions en compagnie d’autres personnalités de votre secteur d’activité.

                                                    {/* <ul>
                                                        <li>Contacter vos prospects avant et pendant le Forum (appli, networking desk...)</li>
                                                        <li>Participer aux sessions en compagnie d&apos;autres personnalités de votre secteur d&apos;activité</li>
                                                        <li>Organiser des conférences de presse, des déjeuners ou des cocktails pour vos clients et vos prospects...</li>
                                                    </ul> */}
                                                </Card.Text>
                                            </div>
                                        </Card.Body>
                                    </Card>

                                    <Carousel className="projectCarousel w-100" interval={3000}>
                                        {adsSecondSlides.map(ads => {
                                            return (
                                                <Carousel.Item key={ads.id}>
                                                    <div className="rounded-0" style={{ height: '350px' }}>
                                                        <Image src={ads.image} alt={ads.alt} layout="fill" loader={imageLoader} />
                                                    </div>
                                                </Carousel.Item>
                                            )
                                        })}
                                    </Carousel>
                                </div>
                            </Col>

                            <Col>
                                <div className="d-flex align-items-center projectCarousel w-100">
                                    <Carousel className="projectCarousel w-100" interval={3000}>
                                        {adsThirtSlides.map(ads => {
                                            return (
                                                <Carousel.Item key={ads.id}>
                                                    <div className="rounded-0" style={{ height: '350px' }}>
                                                        <Image src={ads.image} alt={ads.alt} layout="fill" loader={imageLoader} />
                                                    </div>
                                                </Carousel.Item>
                                            )
                                        })}
                                    </Carousel>

                                    <Card className={CardStyle.horizontal + " shadow w-100"} style={{ height: '350px' }}>
                                        <Card.Body className="p-4">
                                            <div>
                                                <Card.Title className="fw-bold text-uppercase text-primary">
                                                    Merci pour votre soutien !
                                                </Card.Title>

                                                <Card.Text className='mt-3'>
                                                    Chaque année, de prestigieux opérateurs africains et internationaux nous accompagnent et nous soutiennent.
                                                    Nous remercions particulièrement Ecobank, Wave, uniskip, oikocredit, advanscotedivoire, adec-ci,
                                                    digital54nd, visa, deloitte, ecobank, dataprotect, flutterwave, gim-uemoa, sunu assurance,hps-worldwide,
                                                    paydunya, ezipay.africa, omnisliber, boasoncc, julaya, dexterity-africa, connect.myriadgroup, wizallmoney,
                                                    brusselsairlines, turkishairlines, uber, aircotedivoire, medi1tv, canalplusadvertising, epistrophe, financialafrik ...
                                                </Card.Text>
                                            </div>
                                        </Card.Body>
                                    </Card>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Section>

                <Section>
                    <Container>
                        <h1 className="sectionTitle">Nos medias <hr /></h1>

                        <Row xs={1} md={2} lg={3} className="g-5">
                            {youtubeCards.map(youtube => {
                                return (
                                    <Col key={youtube.id}>
                                        <Card className={CardStyle.media + " shadow"}>
                                            <iframe height="2000" src={youtube.link}></iframe>

                                            <Card.Body className="mb-3">
                                                <Card.Title className="fw-bold text-uppercase fs-6">{youtube.title}</Card.Title>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                            })
                            }
                        </Row>
                    </Container>
                </Section>

                <Section className={style.block}>
                    <Container className="text-center">
                        <div className={style.ceoSection}>
                            <h2>Devenir partenaire<hr /></h2>
                            <h1>Pourquoi nous rejoindre ?</h1>

                            <p>
                                Depuis sa creation en 2017 AFF c&apos;est +20 Countries visited to meet ecosystem local players, +100 Speakers from Africa,America,Asia and Europe, USD +5, 000 ,000 of financial flows stimulated by our projects, +5000 Players from fintech community connected to our platforms, +5,000Participants regulators, banks, insurances, fintechs, MNOs, investors, etc., +15 Fintechs creation inspired during Africa Fintech Forum, +200 Startups entrepreneurs, intrapreneurs trained, +5 Advanced training provided to entrepreneurs and intrapreneurs in fintech…
                                Ces initiatives sont soit issues d&apos;Africa Fintech Forum, soit de partenaires avec lesquels nous unissons nos efforts.
                            </p>

                            <Link href={getHref("devenir-partenaire")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">Devenir partenaire</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Partners
