import { NextPage } from 'next'
import React, { useState } from 'react'
import style from './Style.module.css'
import { Head, Header, Section, Menu, Footer } from '../../components'
import { Container, Row, Col, Card } from 'react-bootstrap'
import Image from 'next/image'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight, faUser, faFaceLaughBeam, faMessage, faSynagogue, faHouseMedical, faPhoneSquare, faWineGlass } from "@fortawesome/free-solid-svg-icons"
import Slider from 'react-slick'
import { faWindows, faWordpress, faXbox, faYahoo, faYoutube } from '@fortawesome/free-brands-svg-icons'
import Link from "next/link"
import { getHref, getUrl, imageLoader } from "../../utils"
import { ButtonStyle, CardStyle } from '../../styles'
import img10 from '../../public/apropos/10.png';
import img11 from '../../public/apropos/11.png';

const About: NextPage = () => {
    const statistiquesSliderSettings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoPlaySpeed: 1000
    }

    const statistiquesCards = [
        {
            id: 1,
            title: 'Pays visités',
            value: 20
        },

        {
            id: 2,
            title: 'Intervenants',
            value: 100
        },

        {
            id: 3,
            title: 'USD',
            value: '5M'
        },

        {
            id: 4,
            title: 'Followers',
            value: 5000
        },

        {
            id: 5,
            title: 'Participants',
            value: 5000
        },

        {
            id: 6,
            title: 'Fintechs',
            value: 15
        },

        {
            id: 7,
            title: 'Startups',
            value: 200
        },

        {
            id: 8,
            title: 'Formations avancées',
            value: 5
        },
    ]

    const partnersCards = [
        {
            id: 1,
            image: getUrl('/apropos/7.png')
        },

        {
            id: 2,
            image: getUrl('/apropos/2.png')
        },

        {
            id: 3,
            image: getUrl('/apropos/3.png')
        },

        {
            id: 4,
            image: getUrl('/apropos/4.png')
        },

        {
            id: 5,
            image: getUrl('/apropos/5.png')
        },

        {
            id: 6,
            image: getUrl('/apropos/6.png')
        },
        // {
        //     id: 7,
        //     image: getUrl('/partners/7.jpeg')
        // },

        // {
        //     id: 8,
        //     image: getUrl('/partners/8.jpeg')
        // },

        // {
        //     id: 9,
        //     image: getUrl('/partners/9.jpeg')
        // },

    ]

    const valuesCard = [
        {
            id: 1,
            icon: faUser,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 2,
            icon: faXbox,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 3,
            icon: faWordpress,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 4,
            icon: faFaceLaughBeam,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 5,
            icon: faYoutube,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 6,
            icon: faMessage,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 7,
            icon: faSynagogue,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 8,
            icon: faHouseMedical,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 9,
            icon: faPhoneSquare,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 10,
            icon: faYahoo,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 11,
            icon: faWineGlass,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        },

        {
            id: 12,
            icon: faWindows,
            title: 'Lorem ipsum dolor sit amet',
            content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error at quae dolorem. Repellendus nisi repudiandae odio neque enim odit, ad voluptate accusamus consectetur, commodi corporis expedita sapiente, molestiae magni repellat.'
        }
    ]

    const [activeValueCard, setActiveValueCard] = useState(0)

    return (
        <>
            <Head title='A propos | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="A propos" description='' />

            <main>
                <Section className={style.block}>
                    <div className={style.blockLeft}></div>

                    <div className={style.blockRight}>
                        <div className={style.blockText}>
                            <h2>Notre vision <hr /></h2>
                            <h4>
                                <strong style={{ textAlign: 'justify' }}>AFRICA FINTECH FORUM une organisation internationale à but non lucratif de promotion et
                                    de développement de la finance digitale.
                                </strong>
                            </h4>

                            <p style={{ textAlign: 'justify' }}>
                                Notre but est de construire pour l’Afrique un écosystème fintech basé sur le pavage et le renforcement du
                                réseau local qui rassemble tous les acteurs de l’industrie avec nos partenaires de partout dans le monde.
                                Soutenir les associations locales de fintech en tant qu&apos;agrégateurs de l&apos;écosystème fintech en Afrique, de
                                renforcer la marque et la visibilité de l&apos;AFRICA FINTECH NETWORK
                            </p>
                        </div>
                    </div>
                </Section>

                {/* <Section>
                    <Container>
                        <h1 className="sectionTitle">Nos valeurs <hr /></h1>

                        <Row>
                            <Col xs={1} md={7}>
                                <Row xs={1} md={2} lg={3} className="g-5">
                                    {valuesCard.map((value, index) => {
                                        return (
                                            <Col key={value.id}>
                                                <Card className={CardStyle.transparent + " text-center"} style={{ cursor: 'pointer' }} onClick={() => setActiveValueCard(index)}>
                                                    <Card.Body>
                                                        <FontAwesomeIcon icon={value.icon} size="2x" className="mb-3" />
                                                        <Card.Text>{value.title}</Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        )
                                    })}
                                </Row>
                            </Col>

                            <Col xs={1} md={5}>
                                <Card className="p-3 h-100 text-center" style={{
                                    backgroundColor: 'var(--primary-color)',
                                    color: 'white',
                                    borderRadius: 0
                                }}>
                                    <Card.Body className="border p-5">
                                        <FontAwesomeIcon icon={valuesCard[activeValueCard].icon} size="2x" />
                                        <Card.Text className="my-4">{valuesCard[activeValueCard].title}</Card.Text>
                                        <Card.Text>{valuesCard[activeValueCard].content}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                        <div className="d-flex justify-content-center mt-5">
                            <Link href={getHref("devenir-membre")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">Devenir membre</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section> */}

                <Section className={style.statistiques}>
                    <Container>
                        <h1 className="sectionTitle mb-5 text-white">Nos statistiques <hr /></h1>
                        <div style={{ marginBottom: '5em' }}>
                            <Slider {...statistiquesSliderSettings}>
                                {statistiquesCards.map(statistique => {
                                    return (
                                        <div key={statistique.id}>
                                            <Card className="border-0 rounded-0 text-white" style={{ background: 'none' }}>
                                                <Card.Body className="text-center">
                                                    <Card.Title className="fs-1 fw-bold">+ {statistique.value}</Card.Title>
                                                    <Card.Text><span className="text-uppercase">{statistique.title}</span></Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    )
                                })}
                            </Slider>
                        </div>

                        <h1 className="sectionTitle mb-5 text-white text-uppercase">Nos accomplisements depuis 2017 <hr /></h1>
                        <Row className="text-white mt-4">
                            <div className="col-md-4 col-sm-12">
                                <Card className="border-0" style={{ background: 'none' }}>
                                    <div className="mx-auto">
                                        <Image src={getUrl('/pole-accueil/club.png')} layout="fixed" width={150} height={150} alt="OPEN BANKING AFRICA " loader={imageLoader} />
                                    </div>
                                    <Card.Body className="text-center">
                                        <Card.Title className=" fw-bold text-uppercase">8 Fintech associations </Card.Title>
                                        <Card.Text className="d-flex flex-column text-center mb-5">
                                            <span>initiated, supported , createdCote d’Ivoire, Benin, Togo, Marocco, Niger, RD Congo, Congo, Senegal.</span>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <Card className="border-0" style={{ background: 'none' }}>
                                    <div className="mx-auto">
                                        <Image src={getUrl('/pole-accueil/AFF.png')} layout="fixed" width={150} height={150} alt="Africa Fintech Forum " loader={imageLoader} />
                                    </div>
                                    <Card.Body className="text-center">
                                        <Card.Title className=" fw-bold text-uppercase">Africa Fintech Forum </Card.Title>
                                        <Card.Text className="d-flex flex-column text-center mb-5">
                                            <span>Forum annuel réunissant les acteurs du secteur des fintechs de tous les continents.</span>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <Card className="border-0" style={{ background: 'none' }}>
                                    <div className="mx-auto">
                                        <Image src={getUrl('/pole-accueil/AFF tour.png')} layout="fixed" width={150} height={150} alt="Africa Fintech Tour" loader={imageLoader} />
                                    </div>
                                    <Card.Body className="text-center">
                                        <Card.Title className=" fw-bold text-uppercase">Africa Fintech Tour</Card.Title>
                                        <Card.Text className="d-flex flex-column text-center mb-5">
                                            <span>Voyage annuel en Afrique de l&apos;Ouest, centrale et du Nord pour relier les écosystèmes fintech, créer des synergies et rassembler les énergies..</span>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>

                        </Row>
                        <Row className="text-white justify-content-center">
                            <div className="col-md-4 col-sm-12">
                                <Card className="border-0" style={{ background: 'none' }}>
                                    <div className="mx-auto">
                                        {/* <Card.Img src={getUrl('/pole-accueil/10.png')} /> */}
                                        <Image src={img10} layout="fixed" width={150} height={150} alt="OPEN BANKING AFRICA " loader={imageLoader} />
                                    </div>
                                    <Card.Body className="text-center">
                                        <Card.Title className=" fw-bold text-uppercase">Initiation de la collaboration avec les institutions monétaires et financières</Card.Title>
                                        <Card.Text className="d-flex flex-column text-center mb-5">
                                            <span>Banques centrales, institutions financières de développement, centres financiers internationaux.</span>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="col-md-4 col-sm-12">
                                <Card className="border-0" style={{ background: 'none' }}>
                                    <div className="mx-auto">
                                        <Image src={img11} layout="fixed" width={150} height={150} alt="Participation in workshops and" loader={imageLoader} />
                                    </div>
                                    <Card.Body className="text-center">
                                        <Card.Title className=" fw-bold text-uppercase">Participation à des ateliers et des groupes de travail</Card.Title>
                                        <Card.Text className="d-flex flex-column text-center mb-5">
                                            <span>Sur la stratégie nationale d&apos;inclusion financière, la politique de financement numérique, les réglementations en matière de fintech..</span>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>

                        </Row>
                    </Container>
                </Section>

                <Section>
                    <Container>
                        <Row md={2} className="gx-5">
                            <Col className={style.network}>
                                <h2>Notre réseau <hr /></h2>
                                <h1>Un réseau mondiale en plein expansion</h1>

                                <p style={{ textAlign: 'justify' }} className="mb-3">
                                    Depuis 2017, AFF ne cesse d&apos;accroitre son réseau de partenariat dans sur tout le continent africain et
                                    mondial. Par son ambition d&apos;améliorer l&apos;écosystème fintech africain et créer un vaste réseau de fintech
                                    africain afin de participer au développement de la finance digitale en Afrique, ce sont plus de +20 Pays
                                    visités pour rencontrer les acteurs locaux de l&apos;écosystème, +100 Intervenants d&apos;Afrique, d&apos;Amérique, d&apos;Asie
                                    d&apos;Europe, +5 000 000 USD de flux financiers stimulés par nos projets, +5 000 Acteurs de la communauté
                                    fintech connectés à nos plateformes, +5 000 Participants régulateurs , banques, assurances, fintechs,
                                    ORM, investisseurs, etc., +15 créations de Fintechs inspirées lors du Africa Fintech Forum, +200
                                    entrepreneurs Startups, intrapreneurs formés, +5 formations avancées dispensées aux entrepreneurs et
                                    intrapreneurs en fintech… Sans compter <Link href={getHref('projets-realises')}>les projets mis en place par AFF</Link> pour penser un meilleur futur pour la fintech en Afrique.
                                </p>

                                <Row md={3} className="mb-4 g-4">
                                    {partnersCards.map(partner => {
                                        return (
                                            <Col key={partner.id}>
                                                <Card className="border-0">
                                                    <Card.Img variant="top" src={partner.image} height={120} />
                                                </Card>
                                            </Col>
                                        )
                                    })}
                                </Row>

                                <Link href={getHref("partenaires-aff")}>
                                    <a className={"btn " + ButtonStyle.primary}>
                                        <span className="me-3">Voir plus</span>
                                        <FontAwesomeIcon icon={faAngleRight} />
                                    </a>
                                </Link>
                            </Col>

                            <Col className="d-flex justify-content-center align-items-center">
                                <Image src={getUrl('/network.png')} loader={imageLoader} layout="fixed" alt="Notre réseau" width={700} height={450} />
                            </Col>
                        </Row>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default About
