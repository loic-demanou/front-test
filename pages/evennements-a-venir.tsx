import { NextPage } from "next"
import React, { useState, useEffect } from "react"
import { Footer, Head, Header, Menu, Section } from '../components'
import { Container, Row, Col, Card } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faClock, faLocationDot, faAngleRight, faCalendarAlt } from "@fortawesome/free-solid-svg-icons"
import Link from "next/link"
import { getHref, getApiUrl, getApiImageUrl } from "../utils"
import { ButtonStyle, CardStyle } from "../styles"
import { postProps } from "../props"

const Events: NextPage = () => {
    const [page, setPage] = useState(1)

    const [events, setEvents] = useState<{
        data: Array<postProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    const getEvents = () => {
        setPage(page => page + 1)

        fetch(getApiUrl(`/api/events?page=${page}&limit=6`))
            .then(res => res.json())
            .then(data => setEvents({
                ...events,
                data: [...events.data, ...data.data],
                next_page_url: data.next_page_url
            }))
    }

    useEffect(() => {
        fetch(getApiUrl(`/api/events?limit=6`))
            .then(res => res.json())
            .then(data => setEvents(data))
    }, [setEvents])

    return (
        <>
            <Head title='Évènements à venir | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Évènements à venir"
            description="AFRICA FINTECH FORUM vous propose tout au long de l’année des forums annuels, des conférences
             en ligne ou en présentiels et également des ateliers tout en accompagnants les leaders économiques de l’Afrique en leur apportant témoignages et analyses d’experts sur les grands enjeux et défis 
            auquel le monde des affaires fait face  sur le continent et à travers le monde."
            />

            <main>
                <Section>
                    <Container >
                        <h1 className="sectionTitle">NOS EVENEMENTS DIGITALS <hr /> </h1>

                        <Row className="g-5">
                            {events.data.map(event => {
                                return (
                                    <Col key={event.id} className="col-md-6">
                                        <Card className={CardStyle.horizontalSmall}>
                                            <Row>
                                                <Col className="">
                                                    <Card.Img src={getApiImageUrl(`/event/${event.image}`)} />
                                                </Col>
                                                
                                                <Col className="">
                                                    <Card.Body>
                                                        <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>
                                                        <Card.Text>
                                                            <small>
                                                                <div className="d-flex align-items-center">
                                                                    <FontAwesomeIcon icon={faCalendarAlt} />
                                                                    <span className="ms-2">{event.eventype?.name}</span>
                                                                </div>

                                                                <div className="d-flex align-items-center">
                                                                    <FontAwesomeIcon icon={faLocationDot} />
                                                                    <span className="ms-2">{event.location}</span>
                                                                </div>

                                                                <div className="d-flex align-items-center">
                                                                    <FontAwesomeIcon icon={faClock} />
                                                                    <span className="ms-2">{event.time} UTC</span>
                                                                </div>
                                                            </small>
                                                        </Card.Text>
                                                    </Card.Body>

                                                    <Link href={getHref(`evennement/${event.slug}`)}>
                                                        <a className="stretched-link"></a>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>

                        {events.next_page_url != null && <div className="py-5 d-flex">
                            <button className={"btn " + ButtonStyle.circle} onClick={() => getEvents()} title="Afficher plus">
                                <FontAwesomeIcon icon={faArrowDown} />
                            </button>
                        </div>}

                        <div className="my-5 d-flex justify-content-center">
                            <Link href={getHref("evennements-passes")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">Voir les évènements passés</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Events
