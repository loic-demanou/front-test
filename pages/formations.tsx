import { NextPage } from 'next'
import React from 'react'
import { Container, Row, Col, Card } from "react-bootstrap"
import { Footer, Head, Header, Menu, Section } from '../components'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import Link from "next/link"
import { getHref, getUrl } from "../utils"
import { ButtonStyle, CardStyle } from '../styles'

const Partners: NextPage = () => {
    const eventsCards = [
        {
            id: 1,
            title: 'Magadascar Fintech Forum 2022',
            image: getUrl('/a-la-une/2.jpeg'),
            location: 'Madagascar, Université d\'Antananarivo',
            time: '09:30',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
        },

        {
            id: 2,
            title: 'Africa Fintech Forum',
            image: getUrl('/a-la-une/3.jpg'),
            location: 'Webinar',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        },

        {
            id: 3,
            title: 'Smart Conference',
            image: getUrl('/a-la-une/4.jpg'),
            location: 'Ethiopie, Addis-Abeba',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        }
    ]

    return (
        <>
            <Head title='Formations | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Formations" />

            <main>
                <Section>
                    <Container>
                        <p className="fw-bold text-center mb-5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero perspiciatis voluptatem, porro nam ipsum maiores ea iste ipsam omnis pariatur facere consequatur deleniti placeat laboriosam similique. Earum assumenda quae nesciunt?
                            
                            <hr style={{
                                width: '4em',
                                height: '5px',
                                margin: '0 auto',
                                marginTop: '.8em',
                                opacity: 1,
                            }} />
                        </p>

                        {eventsCards.map((event, index) => {
                            if (index % 2 == 0) {
                                return (
                                    <div key={event.id}>
                                        <Row xs={1} className="g-3">
                                            <Col>
                                                <Card className={CardStyle.horizontal + " shadow"} style={{ height: '350px' }}>
                                                    <Row>
                                                        <Col className="pe-0">
                                                            <Card.Img src={event.image} style={{ height: '350px' }} />
                                                        </Col>
                                                        
                                                        <Col className="ps-0">
                                                            <Card.Body className="h-100 d-flex flex-column justify-content-between">
                                                                <div>
                                                                    <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>
                                                                    <Card.Text>{event.description}</Card.Text>
                                                                </div>

                                                                <div>
                                                                    <Link href={getHref("#")}>
                                                                        <a className={"btn " + ButtonStyle.secondary}>Lien PDF</a>
                                                                    </Link>

                                                                    <Link href={getHref("#")}>
                                                                        <a className={"btn mx-3 " + ButtonStyle.secondary}>Tutoriel</a>
                                                                    </Link>

                                                                    <Link href={getHref("#")}>
                                                                        <a className={"btn mx-1 " + ButtonStyle.primary}>
                                                                            <span className="me-3">Accéder aux cours</span>
                                                                            <FontAwesomeIcon icon={faAngleRight} />
                                                                        </a>
                                                                    </Link>
                                                                </div>
                                                            </Card.Body>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                        </Row>
    
                                        <hr className="my-5" />
                                    </div>
                                )
                            }

                            return (
                                <div key={event.id}>
                                    <Row xs={1} className="g-3">
                                        <Col>
                                            <Card className={CardStyle.horizontal + " shadow"} style={{ height: '350px' }}>
                                                <Row>
                                                    <Col className="ps-1">
                                                        <Card.Body className="ms-2 h-100 d-flex flex-column justify-content-between">
                                                            <div>
                                                                <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>
                                                                <Card.Text>{event.description}</Card.Text>
                                                            </div>

                                                            <div>
                                                                <Link href={getHref("#")}>
                                                                    <a className={"btn " + ButtonStyle.secondary}>Lien PDF</a>
                                                                </Link>

                                                                <Link href={getHref("#")}>
                                                                    <a className={"btn mx-3 " + ButtonStyle.secondary}>Tutoriel</a>
                                                                </Link>

                                                                <Link href={getHref("#")}>
                                                                    <a className={"btn mx-1 " + ButtonStyle.primary}>
                                                                        <span className="me-3">Accéder aux cours</span>
                                                                        <FontAwesomeIcon icon={faAngleRight} />
                                                                    </a>
                                                                </Link>
                                                            </div>
                                                        </Card.Body>
                                                    </Col>

                                                    <Col className="pe-0">
                                                        <Card.Img src={event.image} style={{ height: '350px' }} />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                    </Row>

                                    <hr className="my-5" />
                                </div>
                            )
                        })}
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Partners
