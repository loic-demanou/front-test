import { NextPage } from 'next'
import React from 'react'
import style from './Style.module.css'
import { Head } from '../../components'
import { Row, Col, Form, Button } from "react-bootstrap"
import Image from 'next/image'
import { imageLoader, getUrl, getHref } from '../../utils'
import Link from 'next/link'

const Login: NextPage = () => {
    return (
        <>
            <Head title='Connexion | AFRICA FINTECH FORUM' />

            <main className="min-vh-100 position-relative">
                <div className="position-absolute p-4">
                    <Link href="/">
                        <a>
                            <Image src={getUrl('/logo.png')} loader={imageLoader} layout="fixed" width={180} height={30} alt="AFRICA FINTECH FORUM" title="AFRICA FINTECH FORUM" />
                        </a>
                    </Link>
                </div>

                <Row className="position-absolute h-100 w-100 ms-0">
                    <Col md={4} className="d-flex flex-column justify-content-center" style={{ paddingInline: '6em' }}>
                        <h1 className={style.title}>Connexion <hr /></h1>

                        <p className="mt-3 mb-5">Pas encore mmembre ?
                            <Link href={getHref('devenir-membre')}>
                                <a className='ms-1'>Devenez membre ici</a>
                            </Link>
                        </p>

                        <Form>
                            <Form.Group className="mb-2" controlId="email">
                                <Form.Label>Nom d&apos;utilisateur</Form.Label>
                                <Form.Control type="text" />
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="password">
                                <Form.Label>Mot de passe</Form.Label>
                                <Form.Control type="password" />
                            </Form.Group>

                            <Form.Check type="checkbox" label="Rester connecter" id="remember-me" className="my-3" />

                            <Button variant="primary" type="button" className="w-100">
                                Connexion
                            </Button>
                        </Form>
                    </Col>

                    <Col md={8} className={style.right}></Col>
                </Row>
            </main>
        </>
    )
}

export default Login
