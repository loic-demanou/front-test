import { NextPage } from 'next'
import React from 'react'
import { Container, Row, Col, Card } from "react-bootstrap"
import { Footer, Head, Header, Menu, Section } from '../components'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faClock, faLocationDot } from "@fortawesome/free-solid-svg-icons"
import Link from "next/link"
import { getHref, getUrl } from "../utils"
import { ButtonStyle, CardStyle } from '../styles'

const Partners: NextPage = () => {
    const eventsCards = [
        {
            id: 1,
            title: 'Magadascar Fintech Forum 2022',
            image: getUrl('/a-la-une/2.jpeg'),
            location: 'Madagascar, Université d\'Antananarivo',
            time: '09:30',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
        },

        {
            id: 2,
            title: 'Africa Fintech Forum',
            image: getUrl('/a-la-une/3.jpg'),
            location: 'Webinar',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        },

        {
            id: 3,
            title: 'Smart Conference',
            image: getUrl('/a-la-une/4.jpg'),
            location: 'Ethiopie, Addis-Abeba',
            description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi error non illum. A consequatur amet at mollitia quis autem, culpa dolorem magni sed? Ducimus quia provident quos doloremque, impedit reiciendis.',
            time: '09:30',
        }
    ]
    
    return (
        <>
            <Head title='Bulletin de veille | AFRICA FINTECH FORUM' description='Découvrez toute notre actualité.' />

            <Menu />

            <Header title="Bulletin de veille" />

            <main>
                <Section>
                    <Container>
                        <p className="fw-bold text-center mb-5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero perspiciatis voluptatem, porro nam ipsum maiores ea iste ipsam omnis pariatur facere consequatur deleniti placeat laboriosam similique. Earum assumenda quae nesciunt?
                            
                            <hr style={{
                                width: '4em',
                                height: '5px',
                                margin: '0 auto',
                                marginTop: '.8em',
                                opacity: 1,
                            }} />
                        </p>

                        {eventsCards.map(event => {
                            return (<div key={event.id}>
                                <Row xs={1} md={3} className="g-3" key={event.id}>
                                    <Col>
                                        <Card className="h-100 rounded-0">
                                            <Card.Img variant="top" src={event.image} className="rounded-0" height={250} />
                                        </Card>
                                    </Col>

                                    <Col>
                                        <Card className="rounded-0 border-0 h-100" style={{ background: 'transparent' }}>
                                            <Card.Body>
                                                <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>
                                                <Card.Text>{event.description}</Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>

                                    <Col className="d-flex flex-column justify-content-center">
                                        <Card className={CardStyle.transparent}>
                                            <Card.Body>
                                                <Card.Text>
                                                    <div className="d-flex align-items-center">
                                                        <FontAwesomeIcon icon={faLocationDot} />
                                                        <span className="ms-2">Ethipie, Addis-Abeba</span>
                                                    </div>

                                                    <div className="d-flex align-items-center">
                                                        <FontAwesomeIcon icon={faClock} />
                                                        <span className="ms-2">10h - 12h</span>
                                                    </div>
                                                </Card.Text>

                                                <p>
                                                    <Link href={getHref("#")}>
                                                        <a className={"btn " + ButtonStyle.secondary}>Télécharger le PDF</a>
                                                    </Link>
                                                </p>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>

                                <hr className="my-5" />
                            </div>)
                        })}
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Partners
