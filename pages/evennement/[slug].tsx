import { NextPage, GetStaticPaths, GetStaticProps, GetServerSideProps } from "next"
import Image from "next/image"
import React, { FormEvent, useState } from "react"
import { Container, Row, Col, Card, Form, Carousel } from "react-bootstrap"
import { Footer, Menu, Head, Section} from "../../components"
import { CountryDropdown } from 'react-country-region-selector'
import { imageLoader, getApiUrl, getHref, getApiImageUrl } from "../../utils"
import { CardStyle, ButtonStyle } from "../../styles"
import { postProps } from "../../props"

const PostEvent: NextPage<{event: postProps}> = ({ event }) => {
    const [country, setCountry] = useState('')
    const [civility, setCivility] = useState('M.')

    const handleSubmitForm = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        const form = e.currentTarget

        const formData = new FormData(form)
        formData.append('country', country)

        fetch(getApiUrl(`/api/events/${event.id}`), {
            method: 'post',
            body: formData
        })
            .then(res => res.json())
            .then(data => {
                alert(data.message)
                if (data.status == 'success') form.reset()
            })
    }
    
    return (
        <>
            <Head title={`${event.title}  | AFRICA FINTECH FORUM`} />

            <Menu />

            <main style={{ marginTop: '8em' }}>
                <Section>
                    <Container>
                        <Row className="gx-5">
                            <Col className="col-12" md={7}>
                                <h1 className="fw-bold mb-4" style={{ color: 'var(--primary-color)' }}>{event.title}</h1>
                                <div className="mb-5">{event.content}</div>

                                <Carousel className="mainCarousel" interval={3000}>
                                    {event.image_events.map(image => {
                                        return (
                                            <Carousel.Item key={image.id}>
                                                <div style={{ height: '400px', position: 'relative' }}>
                                                    <Image src={getApiImageUrl(`/event/${image.image}`)} alt={image.image} layout="fill" loader={imageLoader} />
                                                </div>
                                            </Carousel.Item>
                                        )
                                    })}
                                </Carousel>
                            </Col>

                            <Col className="col-12" md={5}>
                                <Card className="shadow">
                                    <Card.Body>
                                        <div className="d-flex justify-content-center">
                                            <p className="fw-bold py-2" style={{ color: 'var(--primary-color)' }}>
                                                Lorem ipsum dolor sit 
                                                
                                                <hr style={{
                                                    width: '4em',
                                                    height: '5px',
                                                    margin: '0 auto',
                                                    marginTop: '.8em',
                                                    opacity: 1,
                                                }} />
                                            </p>
                                        </div>
                                        
                                        <Form onSubmit={handleSubmitForm}>
                                            <Form.Group className="mb-2">
                                                <Form.Label>Civilité</Form.Label>
                                                
                                                <div>
                                                    <Form.Check inline label="M." name="civility" type="radio" value="M." checked={civility == 'M.'} onChange={() => setCivility('M.')} />
                                                    <Form.Check inline label="Mlle" name="civility" type="radio" value="Mlle" checked={civility == 'Mlle'} onChange={() => setCivility('Mlle')} />
                                                    <Form.Check inline label="Mme" name="civility" type="radio" value="Mme" checked={civility == 'Mme'} onChange={() => setCivility('Mme')} />
                                                </div>
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="first_name">
                                                <Form.Label>Prénom(s)</Form.Label>
                                                <Form.Control name="first_name" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="last_name">
                                                <Form.Label>Nom</Form.Label>
                                                <Form.Control name="last_name" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="email">
                                                <Form.Label>Adresse email</Form.Label>
                                                <Form.Control name="email" type="email" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="phone">
                                                <Form.Label>Numéro de téléphone</Form.Label>
                                                <Form.Control name="phone" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="country">
                                                <Form.Label>Pays</Form.Label>
                                                <CountryDropdown name="country" value={country} onChange={country => setCountry(country)} classes="form-select" defaultOptionLabel="Sélectionnez un pays" />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="city">
                                                <Form.Label>Ville</Form.Label>
                                                <Form.Control name="city" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="trade">
                                                <Form.Label>Fonction</Form.Label>
                                                <Form.Control name="trade" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-2" controlId="organisation">
                                                <Form.Label>Organisation</Form.Label>
                                                <Form.Control name="organisation" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="org_sector">
                                                <Form.Label>Secteur de l&apos;organisation</Form.Label>
                                                <Form.Control name="org_sector" type="text" required />
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="image">
                                                <Form.Label>Logo de l&apos;organisation</Form.Label>
                                                <Form.Control name="image" type="file" required />
                                            </Form.Group>

                                            <div className="mb-3 d-flex align-items-center justify-content-center">
                                                <Form.Check label="" name="tos" type="checkbox" required checked />
                                                <span>J&apos;accepte les <a href={getHref('termes-et-conditions')}>Termes de Conditions et de Services</a></span>
                                            </div>

                                            <div className="d-flex justify-content-center">
                                                <button type="submit" className={"btn " + ButtonStyle.secondary}>
                                                    Envoyer
                                                </button>
                                            </div>
                                        </Form>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Section>

                <Section>
                    <h5 className="sectionTitle">Les speakers <hr /> </h5>

                    <div style={{ paddingInline: '5em' }}>
                        <Row xs={1} md={2} lg={4} className="g-3 d-flex justify-content-center">
                            {event.participants.map(participant => {
                                return (
                                    <Col key={participant.id}>
                                        <Card className={CardStyle.people + " shadow"}>
                                            <Card.Img variant="top" src={getApiImageUrl(`/participant/${participant.image}`)} height={300} />

                                            <div style={{ marginTop: '1em' }}>
                                                <div className="text-center">
                                                    <p className="mb-0 fs-5 fw-bold" style={{ color: 'var(--primary-color)' }}>{participant.name}</p>
                                                    <p className="fs-6">{participant.poste}</p>
                                                </div>
                                            </div>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </div>
                </Section>
            </main>

            <Footer />
        </>
    )
}

// export const getStaticPaths: GetStaticPaths = async () => {
//     const res = await fetch(getApiUrl('/api/events'))
//     const events: Array<postProps> = await res.json()

//     const paths = events.map(event  => ({
//         params: { slug: event.slug },
//     }))
    
//     return { paths, fallback: 'blocking' }
// }

// export const getStaticProps: GetStaticProps = async ({ params }) => {
//     const res = await fetch(getApiUrl(`/api/events/${params?.slug}`))
//     const event = await res.json()

//     return {
//         props: { event }
//     }
// }

export default PostEvent

export const getServerSideProps:GetServerSideProps= async ({params}) => {
    const event = await fetch(getApiUrl(`/api/events/${params?.slug}`)).then(r => r.json())

        return {
            props: {
                event
            }
        }
        
    }
