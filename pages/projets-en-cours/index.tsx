import { NextPage } from 'next'
import React, { useState, useEffect } from 'react'
import { Card, Container, Row, Col, Carousel } from 'react-bootstrap'
import { Footer, Menu, Head, Header, Section } from '../../components'
import style from './Style.module.css'
import { faAngleRight, faArrowDown } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { imageLoader, getUrl, getHref, getApiUrl, getApiImageUrl } from '../../utils'
import Image from 'next/image'
import { ButtonStyle } from '../../styles'
import Link from 'next/link'
import { projectProps } from '../../props'

const Projects: NextPage = () => {
    const [page, setPage] = useState(1)

    const [projects, setProjects] = useState<{
        data: Array<projectProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    const getProjects = () => {
        setPage(page => page + 1)

        fetch(getApiUrl(`/api/projects?status=1&page=${page}&limit=3`))
            .then(res => res.json())
            .then(data => setProjects({
                ...projects,
                data: [...projects.data, ...data.data],
                next_page_url: data.next_page_url
            }))
    }

    useEffect(() => {
        fetch(getApiUrl(`/api/projects?status=1&limit=3`))
            .then(res => res.json())
            .then(data => setProjects(data))
    }, [setProjects])

    return (
        <>
            <Head title='Projets en cours | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Nos projets" 
            description='Nos projets en cours de réalisation' />

            <main>
                {/* <Section>
                    <Container className="text-center">
                        <div className={style.ceoSection}>
                            <h2>Lorem ipsum <hr /></h2>
                            <h1>Lorem ipsum dolor sit amet</h1>

                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nobis accusamus, laborum neque doloribus voluptatem consectetur inventore incidunt ducimus soluta natus. Reiciendis ullam iure voluptatem minus numquam temporibus quibusdam optio.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nobis accusamus, laborum neque doloribus voluptatem consectetur inventore incidunt ducimus soluta natus. Reiciendis ullam iure voluptatem minus numquam temporibus quibusdam optio.
                            </p>
                        </div>
                    </Container>
                </Section> */}

                <Section>
                    <h1 className="sectionTitle">DECOUVREZ NOS PROJECTS EN COURS <hr /></h1>

                    <Row xs={1} className="g-5" style={{ paddingInline: '8em' }}>
                        {projects.data.map((project, index) => {
                                if (index % 2 == 0) {
                                    return (
                                        <Col key={project.id}>
                                            <div className="d-flex align-items-center justify-content-center">
                                                <Carousel className="projectCarousel" style={{ width: '45%' }} interval={5000}>
                                                    {project.image_projets.map(image => {
                                                        return (
                                                            <Carousel.Item key={image.id}>
                                                                <div className="rounded-0" style={{ height: '500px' }}>
                                                                    <Image src={getApiImageUrl(`/projet/${image.image}`)} alt={"Slide " + (index + 1)} layout="fill" loader={imageLoader} />
                                                                </div>
                                                            </Carousel.Item>
                                                        )
                                                    })}
                                                </Carousel>

                                                <Card className={"shadow rounded-0 text-white p-2 " + style.background} style={{ width: '55%', height: '600px' }}>
                                                    <Card.Body className="d-flex flex-column justify-content-between">
                                                        <div>
                                                            <Card.Title className={style.projectTitle}>{project.name} <hr /></Card.Title>
                                                            <Card.Text>{project.description}</Card.Text>
                                                            <Card.Text>{project.objectifs}</Card.Text>
                                                        </div>
                                                        
                                                        <Row style={{ paddingInline: '6em' }}>
                                                            {project.partenaires.map(partner => {
                                                                return (
                                                                    <Col key={partner.id}>
                                                                        <Card className="border-0 rounded-0">
                                                                            <Card.Img variant="top" src={getApiImageUrl(`/partenaire/${partner.image}`)} height={60} className="rounded-0" />
                                                                        </Card>
                                                                    </Col>
                                                                )
                                                            })}
                                                        </Row>

                                                        {/* <div className="mt-4">
                                                            <Link href={getHref("#")}>
                                                                <a className={"btn " + ButtonStyle.primary}>
                                                                    <span className="me-3">Appel à soutenir le projet</span>
                                                                    <FontAwesomeIcon icon={faAngleRight} />
                                                                </a>
                                                            </Link>
                                                        </div> */}
                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        </Col>
                                    )
                                }

                                return (
                                    <Col key={project.id}>
                                        <div className="d-flex align-items-center justify-content-center">
                                            <Card className={"shadow rounded-0 text-white p-2 " + style.background} style={{ width: '55%', height: '600px' }}>
                                                <Card.Body className="d-flex flex-column justify-content-between">
                                                    <div>
                                                        <Card.Title className={style.projectTitle}>{project.name} <hr /></Card.Title>
                                                        <Card.Text>{project.description}</Card.Text>
                                                        <Card.Text>{project.objectifs}</Card.Text>
                                                    </div>
                                                    
                                                    <Row style={{ paddingInline: '6em' }}>
                                                        {project.partenaires.map(partner => {
                                                            return (
                                                                <Col key={partner.id}>
                                                                    <Card className="border-0 rounded-0">
                                                                        <Card.Img variant="top" src={getApiImageUrl(`/partenaire/${partner.image}`)} height={60} className="rounded-0" />
                                                                    </Card>
                                                                </Col>
                                                            )
                                                        })}
                                                    </Row>

                                                    {/* <div className="mt-4">
                                                            <Link href={getHref("#")}>
                                                                <a className={"btn " + ButtonStyle.primary}>
                                                                    <span className="me-3">Appel à soutenir le projet</span>
                                                                    <FontAwesomeIcon icon={faAngleRight} />
                                                                </a>
                                                            </Link>
                                                        </div> */}
                                                </Card.Body>
                                            </Card>

                                            <Carousel className="projectCarousel" style={{ width: '45%' }} interval={5000}>
                                                {project.image_projets.map(image => {
                                                    return (
                                                        <Carousel.Item key={image.id}>
                                                            <div className="rounded-0" style={{ height: '500px' }}>
                                                                <Image src={getApiImageUrl(`/projet/${image.image}`)} alt={"Slide " + (index + 1)} layout="fill" loader={imageLoader} />
                                                            </div>
                                                        </Carousel.Item>
                                                    )
                                                })}
                                            </Carousel>
                                        </div>
                                    </Col>
                                )
                        })}
                    </Row>

                    <div>
                        {projects.next_page_url != null && <div className="py-5 d-flex">
                            <button className={"btn " + ButtonStyle.circle} onClick={() => getProjects()} title="Afficher plus">
                                <FontAwesomeIcon icon={faArrowDown} />
                            </button>
                        </div>}
                    </div>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Projects
