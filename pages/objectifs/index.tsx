import { NextPage } from 'next'
import React from 'react'
import { Card, Container, Row, Col } from 'react-bootstrap'
import { Footer, Menu, Head, Header, Section } from '../../components'
import style from './Style.module.css'
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Image from 'next/image'
import { getUrl, imageLoader, getHref } from '../../utils'
import Link from 'next/link'
import { ButtonStyle, CardStyle } from '../../styles'

const Goals: NextPage = () => {
    const valuesCard = [
        {
            id: 1,
            title: 'Plaider',
            content: "Pour la finance numérique auprès des pouvoirs publics et des décideurs politiques, et centrer l'industrie Fintech pour le développement de l'Afrique ."
        },

        {
            id: 2,
            title: 'Développer',
            content: "une éducation adéquate pour adapter les compétences techniques et de gestion à l'innovation financière pour tous les organismes."
        },

        {
            id: 3,
            title: 'Stimuler ',
            content: "les opportunités d'affaires entre les acteurs et construire des ponts de collaboration entre les acteurs de l'écosystème africain et international."
        },

        {
            id: 4,
            title: 'Fournir ',
            content: "et diffuser des clés pour comprendre l'industrie fintech et la développer à travers des études et des recherches."
        },

        {
            id: 5,
            title: 'Construire',
            content: " pour l'Afrique un écosystème fintech basé sur l'ouverture et le renforcement d'un réseau local rassemblant tous les acteurs de l'industrie."
        },

        {
            id: 6,
            title: 'Développer et soutenir',
            content: " des centres d'innovation en Afrique pour tester et accélérer des technologies financières innovantes et percutantes pour l'Afrique."
        },
    ]

    return (
        <>
            <Head title='Objectifs | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Objectifs AFF"
                description='' />

            <main>
                <Section>
                    <Container>
                        <Row md={2}>
                            <Col className={style.ceoSection}>
                                <h2>Des opportunités à saisir <hr /></h2>
                                <h1>Favoriser la croissance de l&apos;écosystème fintech en Afrique et soutenir les associations locales de fintech</h1>

                                <p style={{ textAlign:'justify' }}>
                                    L&apos;objectif du projet est d&apos;intégrer, plutôt que de dupliquer, les preuves existantes sur 
                                    l&apos;écosystème fintech en Afrique, de soutenir les associations locales de fintech en tant 
                                    qu&apos;&apos;agrégateurs de l&apos;écosystème fintech en Afrique, de renforcer la marque et la 
                                    visibilité de l&apos;AFN et de mettre l&apos;accent sur les marchés fintech en dehors des trois 
                                    grands (Nigéria, Afrique du Sud et Kenya) pour rendre visibles les nombreuses opportunités de fintech 
                                    sur le continent auprès des investisseurs, des régulateurs, des décideurs politiques et des partenaires 
                                    de développement
                                </p>
                            </Col>

                            <Col>
                                <Image src={getUrl('/board/1.png')} loader={imageLoader} layout="fixed" alt="Photo Alex SEA" width={450} height={500} />
                            </Col>
                        </Row>

                        <Row xs={1} md={2} lg={3} className="g-5 mt-5">
                            {valuesCard.map(value => {
                                return (
                                    <Col key={value.id}>
                                        <Card className={CardStyle.transparent}>
                                            <Card.Body>
                                                <Card.Title className="fw-bold text-uppercase fs-6" style={{ color: 'var(--primary-color)' }}>{value.title}</Card.Title>
                                                <Card.Text >{value.content}</Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </Container>
                </Section>

                <Section className={style.block}>
                    <Container>
                        <h1 className="sectionTitle mb-0">ENTREPRISES, DÉCOUVREZ COMMENT COLLABORER <br /> AVEC AFRICA FINTECH FORUM</h1>

                        <div className="mt-5 d-flex justify-content-center">
                            <Link href={getHref("devenir-partenaire")}>
                                <a className={"btn " + ButtonStyle.primary}>
                                    <span className="me-3">Devenir partenaire</span>
                                    <FontAwesomeIcon icon={faAngleRight} />
                                </a>
                            </Link>
                        </div>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Goals
