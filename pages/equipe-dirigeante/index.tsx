import { NextPage } from 'next'
import React from 'react'
import style from './Style.module.css'
import { Head, Header, Section, Menu, Footer } from '../../components'
import { Container, Row, Col, Card } from 'react-bootstrap'
import Image from 'next/image'
import { getUrl, imageLoader } from "../../utils"
import { CardStyle } from '../../styles'

const Team: NextPage = () => {
    const leadersCards = [
        {
            id: 1,
            image: getUrl('/board/KATHARINA DAKLA.jpg'),
            name: 'KATHARINA DAKLA',
            job: 'Fondatrice et PDG de StellarOne - Strategy and Investment advisory in Tech'
        },

        {
            id: 2,
            image: getUrl('/board/Yves komaclo.jpg'),
            name: 'Yves komaclo',
            job: "Directeur d'Investissement Afrique de l'Ouest (Investment Manager), Oikocredit"
        },

        {
            id: 3,
            image: getUrl('/board/Roseline Emma Rasolovoahangy.jpg'),
            name: 'Roseline Emma Rasolovoahangy',
            job: 'President petromad Inc'
        },

        {
            id: 4,
            image: getUrl('/board/Bruner Nozière.jpg'),
            name: 'Bruner Nozière',
            job: 'Co-Founder Netdollar'
        },

        {
            id: 5,
            image: getUrl('/board/Matteo Rizzi.jpg'),
            name: 'Matteo Rizzi',
            job: 'Venture Partner at Bamboo Capital Partners | Co-Founder at FTSGroup.eu | Published Author - "Fintech Revolution" (2016) and "Talents & Rebels" (2019)'
        },

        {
            id: 6,
            image: getUrl('/board/MARIEME NDIAYE.jpg'),
            name: 'MARIEME NDIAYE',
            job: 'Senior expert payment Fintech'
        },

        {
            id: 7,
            image: getUrl('/board/Andrew Davis.jpg'),
            name: 'Andrew Davis',
            job: 'The Fintech Institute Executive Director '
        },

        {
            id: 8,
            image: getUrl('/board/Michael salmony.jpg'),
            name: 'Michael salmony',
            job: ' equensWorldline SE Germany, Executive Adviser '
        },

        {
            id: 9,
            image: getUrl('/board/Estelle brack.jpg'),
            name: 'Estelle brack',
            job: 'Founder and CEO of KiraliT '
        },
        {
            id: 10,
            image: getUrl('/board/hafou touré.jpg'),
            name: 'hafou touré',
            job: 'Founder HTS Partners  '
        },

        {
            id: 11,
            image: getUrl('/board/Yassine Regragui.jpg'),
            name: 'Yassine Regragui',
            job: 'Fintech & China Expert  ex-Deloitte, Alipay, Alibaba '
        },
        {
            id: 12,
            image: getUrl('/board/Josué Toho.jpg'),
            name: 'Josué Toho',
            job: 'Inclusive Finance & Fintech Specialist'
        },

        {
            id: 13,
            image: getUrl('/board/Babacar Diallo.jpg'),
            name: 'Babacar Diallo',
            job: 'Consultant Inclusion Specialist Financial, West Africa region'
        },
        {
            id: 14,
            image: getUrl('/board/Jaures Tcheou.jpg'),
            name: 'Jaures Tcheou',
            job: 'CEO Capitaux Reunis et AfreeMonex'
        },

        {
            id: 15,
            image: getUrl('/board/Abdelkader yousfi.jpg'),
            name: 'Abdelkader yousfi',
            job: 'Founder and CEO of sprinthub '
        },
        {
            id: 16,
            image: getUrl('/board/DRAMANE MEITE.jpg'),
            name: 'DRAMANE MEITE',
            job: "Professionnel de l'investissement et des services financiers"
        },

        {
            id: 17,
            image: getUrl('/board/Sory TOURE.jpg'),
            name: 'Sory TOURE',
            job: 'CEO DEXTERITY AFRICA'
        },
        {
            id: 18,
            image: getUrl('/board/Devinder Singh.jpg'),
            name: 'Devinder Singh',
            job: 'ERICSSON, Head of Mobile Money CU MTN West'
        },
        {
            id: 19,
            image: getUrl('/board/Sidi-Mohamed Dhaker.jpg'),
            name: 'Sidi-Mohamed Dhaker',
            job: 'Conseiller du Gouverneur Banque Centrale de Mauritanie'
        },

        {
            id: 20,
            image: getUrl('/board/Karine Mazand-Mboumba Tchitoula.jpg'),
            name: 'Karine Mazand-Mboumba Tchitoula',
            job: 'MMT Avocats Avocat-Legal designer'
        },
    ]

    return (
        <>
            <Head title='Equipe dirigeante | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Equipe dirigeante" 
            description="L’équipe de AFF est multidisciplinaire pour répondre aux besoins des acteurs de l'industrie sur au niveau local comme international." />

            <main>
                <Section>
                    <Container>
                        <Row md={2}>
                            <Col className={style.block}>
                                <h2>Le mot de... <hr /></h2>
                                <h1>Alex Sea, co-Founder and director, FINOV AFRICA, Côte d’Ivoire</h1>

                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nobis accusamus, laborum neque doloribus voluptatem consectetur inventore incidunt ducimus soluta natus. Reiciendis ullam iure voluptatem minus numquam temporibus quibusdam optio.
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, nobis accusamus, laborum neque doloribus voluptatem consectetur inventore incidunt ducimus soluta natus. Reiciendis ullam iure voluptatem minus numquam temporibus quibusdam optio.
                                </p>
                            </Col>

                            <Col>
                                <Image src={getUrl('/board/1.png')} loader={imageLoader} layout="fixed" alt="Photo Alex SEA" width={450} height={500} />
                            </Col>
                        </Row>
                    </Container>
                </Section>

                <Section>
                    <Container>
                        <h1 className="sectionTitle">Découvrez l&apos;équipe <hr /></h1>

                        <Row xs={1} md={2} lg={4} className="g-5 d-flex justify-content-center">
                            {leadersCards.map(leader => {
                                return (
                                    <Col key={leader.id}>
                                        <Card className={CardStyle.people + " mx-0 shadow h-100"}>
                                            <Card.Img variant="top" src={leader.image} height={300} />

                                            <div style={{ marginTop: '1em' }}>
                                                <div className="text-center">
                                                    <p className="mb-0 fs-5 fw-bold" style={{ color: 'var(--primary-color)' }}>{leader.name}</p>
                                                    <p className="fs-6">{leader.job}</p>
                                                </div>
                                            </div>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Team