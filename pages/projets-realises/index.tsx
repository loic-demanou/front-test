import { NextPage } from 'next'
import React, { useState, useEffect } from 'react'
import { Card, Container, Row, Col, Carousel } from 'react-bootstrap'
import { Footer, Menu, Head, Header, Section } from '../../components'
import style from './Style.module.css'
import { imageLoader, getUrl, getApiImageUrl } from '../../utils'
import Image from 'next/image'
import { projectProps } from '../../props'
import { getApiUrl } from '../../utils'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown } from "@fortawesome/free-solid-svg-icons"
import { ButtonStyle } from '../../styles'

const ProjectsDone: NextPage = () => {
    const [page, setPage] = useState(1)

    const [projects, setProjects] = useState<{
        data: Array<projectProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    const getProjects = () => {
        setPage(page => page + 1)

        fetch(getApiUrl(`/api/projects?status=0&page=${page}&limit=3`))
            .then(res => res.json())
            .then(data => setProjects({
                ...projects,
                data: [...projects.data, ...data.data],
                next_page_url: data.next_page_url
            }))
    }

    useEffect(() => {
        fetch(getApiUrl(`/api/projects?status=0&limit=3`))
            .then(res => res.json())
            .then(data => setProjects(data))
    }, [setProjects])

    return (
        <>
            <Head title='Projets réalisés | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Nos projets"
                description='Nos projets réalisés pour un écosystème fintech africain plus vaste' />

            <main>
                <Section>
                    <Container className="text-center">
                        <div className={style.ceoSection}>
                            <h2>NOTRE AMBITION <hr /></h2>
                            <h1 style={{ fontSize:'23px' }}>Passer à l&apos;action pour participer au développement de la finance digitale en Afrique.</h1>

                            <p>
                                Des projets sont élaborés et mis en place de façon constructive pour développer un écosystème fintech
                                plus solide et favoriser un réseau Africa Fintech à travers le continent rassemblant tous les acteurs
                                de l’industrie avec nos partenaires de partout dans le monde.
                            </p>
                        </div>
                    </Container>
                </Section>

                <Section>
                    <h1 className="sectionTitle">DECOUVREZ NOS PROJECTS RÉALISÉS <hr /></h1>

                    <Row xs={1} className="g-5" style={{ paddingInline: '8em' }}>
                        {projects.data.map((project, index) => {
                            if (index % 2 == 0) {
                                return (
                                    <Col key={project.id}>
                                        <div className="d-flex align-items-center justify-content-center">
                                            <Carousel className="projectCarousel" style={{ width: '45%' }} interval={5000}>
                                                {project.image_projets.map(image => {
                                                    return (
                                                        <Carousel.Item key={image.id}>
                                                            <div className="rounded-0" style={{ height: '500px' }}>
                                                                <Image src={getApiImageUrl(`/projet/${image.image}`)} alt={"Slide " + (index + 1)} layout="fill" loader={imageLoader} />
                                                            </div>
                                                        </Carousel.Item>
                                                    )
                                                })}
                                            </Carousel>

                                            <Card className={"shadow rounded-0 text-white p-2 " + style.background} style={{ width: '55%', height: '600px' }}>
                                                <Card.Body className="d-flex flex-column justify-content-between">
                                                    <div>
                                                        <Card.Title className={style.projectTitle}>{project.name} <hr /></Card.Title>
                                                        <Card.Text>{project.description}</Card.Text>
                                                        <Card.Text>{project.objectifs}</Card.Text>
                                                    </div>

                                                    <div>
                                                        <p className={style.statistiques}><hr /> Statistiques</p>

                                                        <Row className="my-4">
                                                            {project.statistiques.map(statistic => {
                                                                return (<Col key={statistic.id} className="d-flex justify-content-between">
                                                                    <div className="d-flex flex-column align-items-center w-100">
                                                                        <span className="fw-bold fs-4">25</span>
                                                                        <span>Lorem ipsum</span>
                                                                    </div>

                                                                    <div className="vr px-0"></div>
                                                                </Col>)
                                                            })}
                                                        </Row>
                                                    </div>

                                                    <Row style={{ paddingInline: '6em' }}>
                                                        {project.partenaires.map(partner => {
                                                            return (
                                                                <Col key={partner.id}>
                                                                    <Card className="border-0 rounded-0">
                                                                        <Card.Img variant="top" src={getApiImageUrl(`/partenaire/${partner.image}`)} height={60} className="rounded-0" />
                                                                    </Card>
                                                                </Col>
                                                            )
                                                        })}
                                                    </Row>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Col>
                                )
                            }

                            return (
                                <Col key={project.id}>
                                    <div className="d-flex align-items-center justify-content-center">
                                        <Card className={"shadow rounded-0 text-white p-2 " + style.background} style={{ width: '55%', height: '600px' }}>
                                            <Card.Body className="d-flex flex-column justify-content-between">
                                                <div>
                                                    <Card.Title className={style.projectTitle}>{project.name} <hr /></Card.Title>
                                                    <Card.Text>{project.description}</Card.Text>
                                                    <Card.Text>{project.objectifs}</Card.Text>
                                                </div>

                                                <div>
                                                    <p className={style.statistiques}><hr /> Statistiques</p>

                                                    <Row className="my-4">
                                                        {project.statistiques.map(statistic => {
                                                            return (<div key={statistic.id}>
                                                                <Col className="d-flex flex-column align-items-center">
                                                                    <span className="fw-bold fs-4">25</span>
                                                                    <span>Lorem ipsum</span>
                                                                </Col>

                                                                <div className="vr px-0"></div>
                                                            </div>)
                                                        })}
                                                    </Row>
                                                </div>

                                                <Row style={{ paddingInline: '6em' }}>
                                                    {project.partenaires.map(partner => {
                                                        return (
                                                            <Col key={partner.id}>
                                                                <Card className="border-0 rounded-0">
                                                                    <Card.Img variant="top" src={getApiImageUrl(`/partenaire/${partner.image}`)} height={60} className="rounded-0" />
                                                                </Card>
                                                            </Col>
                                                        )
                                                    })}
                                                </Row>
                                            </Card.Body>
                                        </Card>

                                        <Carousel className="projectCarousel" style={{ width: '45%' }} interval={5000}>
                                            {project.image_projets.map(image => {
                                                return (
                                                    <Carousel.Item key={image.id}>
                                                        <div className="rounded-0" style={{ height: '500px' }}>
                                                            <Image src={getApiImageUrl(`/projet/${image.image}`)} alt={"Slide " + (index + 1)} layout="fill" loader={imageLoader} />
                                                        </div>
                                                    </Carousel.Item>
                                                )
                                            })}
                                        </Carousel>
                                    </div>
                                </Col>
                            )
                        })}
                    </Row>

                    <div>
                        {projects.next_page_url != null && <div className="py-5 d-flex">
                            <button className={"btn " + ButtonStyle.circle} onClick={() => getProjects()} title="Afficher plus">
                                <FontAwesomeIcon icon={faArrowDown} />
                            </button>
                        </div>}
                    </div>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default ProjectsDone
