import { NextPage } from "next"
import React, { useState, useEffect } from "react"
import { Footer, Head, Header, Menu, Section } from '../components'
import { Container, Row, Col, Card, Spinner } from "react-bootstrap"
import InfiniteScroll from "react-infinite-scroll-component"
import Link from "next/link"
import { getHref, truncateText, getApiUrl, getApiImageUrl } from "../utils"
import { CardStyle } from "../styles"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faClock, faLocationDot, faCalendarAlt } from "@fortawesome/free-solid-svg-icons"
import { postProps } from "../props"

const News: NextPage = () => {
    const [page, setPage] = useState(1)

    const [posts, setPosts] = useState<{
        data: Array<postProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    const getPosts = () => {
        setPage(page => page + 1)

        fetch(getApiUrl(`/api/news/actualite?page=${page}&limit=4`))
            .then(res => res.json())
            .then(data => setPosts({
                ...posts,
                data: [...posts.data, ...data.data],
                next_page_url: data.next_page_url
            }))
    }

    useEffect(() => {
        fetch(getApiUrl(`/api/news/actualite?limit=4`))
            .then(res => res.json())
            .then(data => setPosts(data))
    }, [setPosts])

    return (
        <>
            <Head title='Actualité | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Actualité" description="Retrouvez toutes nos actualités et suivez nos différentes activités" />

            <main>
                <Section>
                    <Container>
                        <h1 className="sectionTitle">Découvrez nos actualités <hr /> </h1>

                        <InfiniteScroll
                            dataLength={posts.data.length}
                            next={() => getPosts()}
                            hasMore={true}
                            loader={
                                posts.next_page_url != null && <div className="d-flex align-items-center justify-content-center py-4">
                                    <Spinner animation="border" variant="primary" />
                                </div>
                            }
                        >
                            <Row xs={1} md={2} className="g-5">
                                {posts.data.map(post => {
                                    if (!post.eventype) {
                                        return (
                                            <Col key={post.id}>
                                                <Card className={CardStyle.horizontal} style={{ height: '250px' }}>
                                                    <Row>
                                                        <Col style={{ paddingRight: 0 }}>
                                                            <Card.Img src={getApiImageUrl(`/news/${post.image}`)} style={{ height: '250px' }} />
                                                        </Col>
                                                        
                                                        <Col style={{ paddingLeft: 0 }}>
                                                            <Card.Body>
                                                                <Card.Text>
                                                                    <small>{new Date(post.created_at).toLocaleDateString('fr-FR', {year: 'numeric', day: 'numeric', month: 'short'})} / {post.newstype.name}</small>
                                                                </Card.Text>
    
                                                                <Card.Title className="fw-bold text-uppercase">
                                                                    <small>{post.title}</small>
                                                                </Card.Title>
    
                                                                <Card.Text>
                                                                    <small>{truncateText(post.content, 150)}</small>
                                                                </Card.Text>
                                                            </Card.Body>
    
                                                            <Link href={getHref(`article/${post.slug}`)}>
                                                                <a className="stretched-link"></a>
                                                            </Link>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                        )
                                    }

                                    return (
                                        <Col key={post.id}>
                                            <Card className={CardStyle.horizontal} style={{ height: '250px' }}>
                                                <Row>
                                                    <Col className="pe-0">
                                                        <Card.Img src={getApiImageUrl(`/event/${post.image}`)} style={{ height: '250px' }} />
                                                    </Col>
                                                    
                                                    <Col className="ps-0">
                                                        <Card.Body>
                                                            <Card.Title className="fw-bold text-uppercase">{post.title}</Card.Title>
                                                            <Card.Text>
                                                                <small>
                                                                    <div className="d-flex align-items-center">
                                                                        <FontAwesomeIcon icon={faCalendarAlt} />
                                                                        <span className="ms-2">{post.eventype.name}</span>
                                                                    </div>

                                                                    <div className="d-flex align-items-center">
                                                                        <FontAwesomeIcon icon={faLocationDot} />
                                                                        <span className="ms-2">{post.location}</span>
                                                                    </div>

                                                                    <div className="d-flex align-items-center">
                                                                        <FontAwesomeIcon icon={faClock} />
                                                                        <span className="ms-2">{post.time} UTC</span>
                                                                    </div>
                                                                </small>
                                                            </Card.Text>
                                                        </Card.Body>

                                                        <Link href={getHref(`evennement/${post.slug}`)}>
                                                            <a className="stretched-link"></a>
                                                        </Link>
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                    )
                                })}
                            </Row>
                        </InfiniteScroll>
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default News
