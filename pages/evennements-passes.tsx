import { NextPage } from "next"
import React, { useState, useEffect } from "react"
import { Footer, Head, Header, Menu, Section } from '../components'
import { Container, Row, Col, Card } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faCalendarAlt } from "@fortawesome/free-solid-svg-icons"
import Link from "next/link"
import { getApiUrl, getHref, getApiImageUrl } from "../utils"
import { ButtonStyle, CardStyle } from "../styles"
import { postProps } from "../props"

const Events: NextPage = () => {
    const [page, setPage] = useState(1)

    const [events, setEvents] = useState<{
        data: Array<postProps>,
        next_page_url: '' | null
    }>({
        data: [],
        next_page_url: null
    })

    const getEvents = () => {
        setPage(page => page + 1)

        fetch(getApiUrl(`/api/events?page=${page}&limit=4&type=old`))
            .then(res => res.json())
            .then(data => setEvents({
                ...events,
                data: [...events.data, ...data.data],
                next_page_url: data.next_page_url
            }))
    }

    useEffect(() => {
        fetch(getApiUrl(`/api/events?limit=4&type=old`))
            .then(res => res.json())
            .then(data => setEvents(data))
    }, [setEvents])

    return (
        <>
            <Head title='Évènements passés | AFRICA FINTECH FORUM' />

            <Menu />

            <Header title="Évènements passés"
            description="Revivez nos derniers evenements en images et en videos ainsi
            vous acquierir des rapports des themes qui ont ete abordé lors de ses moments unique." />

            <main>
                <Section>
                    <Container style={{ paddingInline: '10em' }}>
                        <h1 className="sectionTitle">NOS EVENEMENTS DIGITALS <hr /> </h1>

                        <Row xs={1} className="g-5">
                            {events.data.map((event, index) => {
                                if (index % 2 == 0) {
                                    return (
                                        <Col key={event.id}>
                                            <Card className={CardStyle.horizontal}>
                                                <Row>
                                                    <Col style={{ paddingRight: 0 }}>
                                                        <Card.Img src={getApiImageUrl(`/event/${event.image}`)} />
                                                    </Col>
                                                    
                                                    <Col className="d-flex align-items-center justify-content-center ps-0">
                                                        <Card.Body>
                                                            <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>
                                                            
                                                            <div className="d-flex align-items-center my-2">
                                                                <FontAwesomeIcon icon={faCalendarAlt} />
                                                                <span className="ms-2">{event.eventype?.name}</span>
                                                            </div>
                                                            
                                                            <Card.Text>{event.content}</Card.Text>

                                                            <Card.Text>
                                                                <Link href={getHref("#")}>
                                                                    <a>Lorem ipsum dolor sit amet</a>
                                                                </Link>
                                                            </Card.Text>
                                                        </Card.Body>
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                    )
                                }

                                return (
                                    <Col key={event.id}>
                                        <Card className={CardStyle.horizontal}>
                                            <Row>
                                                <Col className="d-flex align-items-center justify-content-center pe-0">
                                                    <Card.Body>
                                                        <Card.Title className="fw-bold text-uppercase">{event.title}</Card.Title>

                                                        <div className="d-flex align-items-center my-2">
                                                            <FontAwesomeIcon icon={faCalendarAlt} />
                                                            <span className="ms-2">{event.eventype?.name}</span>
                                                        </div>

                                                        <Card.Text>{event.content}</Card.Text>

                                                        <Card.Text>
                                                            <Link href={getHref("#")}>
                                                                <a>Lorem ipsum dolor sit amet</a>
                                                            </Link>
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Col>
                                                
                                                <Col style={{ paddingRight: 0 }}>
                                                    <Card.Img src={getApiImageUrl(`/event/${event.image}`)} />
                                                </Col>
                                            </Row>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                        
                        {events.next_page_url != null && <div className="py-5 d-flex">
                            <button className={"btn " + ButtonStyle.circle} onClick={() => getEvents()} title="Afficher plus">
                                <FontAwesomeIcon icon={faArrowDown} />
                            </button>
                        </div>}
                    </Container>
                </Section>
            </main>

            <Footer />
        </>
    )
}

export default Events
